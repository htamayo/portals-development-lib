package co.development.portals.files;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.portlet.ResourceResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.StringPool;

import co.development.portals.types.NumberUtil;

/**
 * 
 * @author H. Tamayo 
 * @last_edition 8/06/2019
 *
 */
public class ExcelUtil {
	//
	public static HSSFWorkbook createExcelWorkbook() {
		return new HSSFWorkbook();
	}
	//
	public static void downloadExcel(HSSFWorkbook workbook, 
			String fileName, ResourceResponse resourceResponse) 
					throws IOException{
		resourceResponse.setContentType(
				"application/vnd.ms-excel");
		resourceResponse.addProperty(
				HttpHeaders.CONTENT_DISPOSITION, 
				"filename=\"" + fileName + "\"");
		//
		workbook.write(
				resourceResponse.getPortletOutputStream());
	}
	//
	public static <T> HSSFWorkbook populateExcelSheetWorkbook(
			HSSFWorkbook workbook, List<String> headers, 
			Class<T> dataClass, List<T> data) {
		return populateExcelSheetWorkbook(workbook, 0, headers, 
				dataClass, data, null, null);
	}
	//
	public static <T> HSSFWorkbook populateExcelSheetWorkbook(
			HSSFWorkbook workbook, List<String> headers, 
			Class<T> dataClass, List<T> data, 
			List<String> excludingFields) {
		return populateExcelSheetWorkbook(workbook, 0, headers, 
				dataClass, data, excludingFields, null);
	}
	//
	public static <T> HSSFWorkbook populateExcelSheetWorkbook(
			HSSFWorkbook workbook, List<String> headers, 
			Class<T> dataClass, List<T> data, 
			Map<String, FormatType> formatFields) {
		return populateExcelSheetWorkbook(workbook, 0, headers, 
				dataClass, data, null, formatFields);
	}
	//
	public static <T> HSSFWorkbook populateExcelSheetWorkbook(
			HSSFWorkbook workbook, List<String> headers, 
			Class<T> dataClass, List<T> data, 
			List<String> excludingFields, 
			Map<String, FormatType> formatFields) {
		return populateExcelSheetWorkbook(workbook, 0, headers, 
				dataClass, data, excludingFields, formatFields);
	}
	//
	public static <T> HSSFWorkbook populateExcelSheetWorkbook(
			HSSFWorkbook workbook, int sheetIndex, 
			List<String> headers, Class<T> dataClass, 
			List<T> data, List<String> excludingFields, 
			Map<String, FormatType> formatFields) {
		HSSFSheet sheet = workbook.createSheet("Hoja "
				+ (sheetIndex + 1));
		//
		int columnIndex = 0, rowIndex = 0;
		HSSFRow row = sheet.createRow(rowIndex);
		
		for (String header : headers) {
			Cell cell = row.createCell(columnIndex);
			cell.setCellValue(header);
			columnIndex++;
		}
		//
		columnIndex = 0;
		rowIndex++;
		//
		Field[] fields = dataClass.getDeclaredFields();
		//
		for (Object dataObject : data) {
			row = sheet.createRow(rowIndex);
			//
			for (int index=0; index < fields.length; index++) {
				Field field = fields[index];
				try {
					field.setAccessible(true);
					//
					if (excludingFields != null) 
						if (excludingFields.contains(
								field.getName()))
							continue;
					//
					Object value = null;
					//
					if (formatFields != null) {
						FormatType formatType = 
								formatFields.get(field.getName());
						//
						if (formatType != null) {
							if (Format.COP_CURRENCY.equals(
									formatType.getFormat()) || 
									Format.USD_CURRENCY.equals(
											formatType.getFormat())) {
								String currency = formatType.getCurrency();
								//
								if (BigDecimal.class.equals(
										formatType.getClazz())) {
									value = NumberUtil.formatCurrency(
											(BigDecimal)field.get(dataObject), 
											currency);
								} else if (Double.class.equals(
										formatType.getClazz())) {
									value = NumberUtil.formatCurrency(
											(Double)field.get(dataObject), 
											currency);
								}
							} else if (Format.COP_NUMBER.equals(
									formatType.getFormat()) || 
									Format.USD_NUMBER.equals(
											formatType.getFormat())) {
								String currencyId = formatType.getCurrency();
								//
								if (BigDecimal.class.equals(
										formatType.getClazz())) {
									value = NumberUtil.formatNumber(
											(BigDecimal)field.get(dataObject), 
											currencyId);
								} else if (Double.class.equals(
										formatType.getClazz())) {
									value = NumberUtil.formatNumber(
											(Double)field.get(dataObject), 
											currencyId);
								}
							} else if (Format.INT.equals(formatType.getFormat())) {
								if (BigDecimal.class.equals(
										formatType.getClazz())) {
									value = ((BigDecimal)field.get(dataObject)).intValue();
								} else if (Double.class.equals(
										formatType.getClazz())) {
									value = ((Double)field.get(dataObject)).intValue();
								}
							} else if (Format.PREV_DINAMIC_CURRENCY.equals(
									formatType.getFormat())) {
								Field currencyField = fields[index - 1];
								String currency = currencyField.get(
										dataObject).toString();
								//
								if (BigDecimal.class.equals(
										formatType.getClazz())) {
									value = NumberUtil.formatCurrency(
											(BigDecimal)field.get(dataObject), 
											currency);
								} else if (Double.class.equals(
										formatType.getClazz())) {
									value = NumberUtil.formatCurrency(
											(Double)field.get(dataObject), 
											currency);
								}
							} else if (Format.PREV_DINAMIC_NUMBER.equals(formatType.getFormat())) {
								Field currencyField = fields[(index - 1)];
								String currency = (String)currencyField.get(
										dataObject);
								//
								if (BigDecimal.class.equals(
										formatType.getClazz())) {
									value = NumberUtil.formatNumber(
											(BigDecimal)field.get(dataObject), 
											currency);
								} else if (Double.class.equals(
										formatType.getClazz())) {
									value = NumberUtil.formatNumber(
											(Double)field.get(dataObject), 
											currency);
								}
							}
						}
							
					}
					//
					if (value == null) {
						value = field.get(dataObject);
					}
					//
					Cell cell = row.createCell(columnIndex);
					cell.setCellValue(String.valueOf(value));
					columnIndex++;
				} catch (IllegalAccessException ex) {
					String msg = "Error in populateExcelSheetWorkbook"
							+ " accesing to the value of " 
							+ dataClass.getSimpleName() + "." 
							+ field.getName() + ": " 
							+ ex.getMessage();
					//
					if (_log.isDebugEnabled()) 
						_log.error(msg, ex);
					else 
						_log.error(msg);
				}
			}
			//
			columnIndex = 0;
			rowIndex++;
		}
		//
		return workbook;
	}
	//
	private static Log _log = LogFactoryUtil.getLog(ExcelUtil.class);
	//
	/**
	 * 
	 * @author H. Tamayo 
	 * @last_edition 12/04/2019
	 *
	 */
	public static class FormatType {
		//
		// #region Properties
		private Class<?> clazz;
		private Format format;
		// #endregion
		//
		// #region Constructor
		public FormatType(Class<?> clazz, Format format) {
			super();
			this.clazz = clazz;
			this.format = format;
		}
		// #endregion
		//
		// #region Getters and Setters
		public Class<?> getClazz() {
			return clazz;
		}
		public void setClazz(Class<?> clazz) {
			this.clazz = clazz;
		}
		public String getCurrency() {
			if (Format.USD_CURRENCY.equals(this.format) || 
					Format.USD_NUMBER.equals(this.format)) {
				return NumberUtil.USD_CURRENCY;
			} else if (Format.COP_CURRENCY.equals(this.format) || 
					Format.COP_NUMBER.equals(this.format)) {
				return NumberUtil.COP_CURRENCY;
			}
			//
			return StringPool.BLANK;
		}
		public Format getFormat() {
			return format;
		}
		public void setFormat(Format format) {
			this.format = format;
		}
		// #endregion
		//
	}
	//
	/**
	 * 
	 * @author H. Tamayo 
	 * @last_edition 12/04/2019
	 *
	 */
	public static enum Format {
		COP_CURRENCY, COP_NUMBER, PREV_DINAMIC_CURRENCY, PREV_DINAMIC_NUMBER, USD_CURRENCY, USD_NUMBER, INT
	}
	//
}
