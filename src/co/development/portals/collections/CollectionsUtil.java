package co.development.portals.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author H. Tamayo
 * @editor H. Tamayo
 * @last edition 27 de oct. de 2016
 *
 */
public class CollectionsUtil {

	public static <T> List<T> getUniqueItemsInOriginalOrder(
			List<T> list) {
		Set<T> set = new LinkedHashSet<T>(list);
		//
		list.clear();
		list.addAll(set);
		//
		return list;
	}
	
	public static <T> List<T> getUniqueItems(List<T> list) {
		Set<T> set = new HashSet<T>();
		//
		list.clear();
		list.addAll(set);
		//
		return list;
	}
	
	public static <T> List<T> getCommonObjects(
			List<T> list, T item, T nextItem, Comparator comparator) {
		Collections.sort(list, comparator);
		//
		List<T> returnList = new ArrayList<T>();
		//
		if (list != null) {
			if (list.size() > 0) {
				int fromIndex = 0, toIndex = 0;
				//
				fromIndex = Collections.binarySearch(
						list, item, comparator);
				//
				if (nextItem != null) 
					toIndex = Collections.binarySearch(
							list, nextItem, comparator);
				else
					toIndex = list.size() - 1;
				//
				return list.subList(fromIndex, toIndex);
			}
			//
			return returnList;
		} else {
			throw new NullPointerException(
					"The List that containst the Objects, not can be empty.");
		}
	}
	
}
