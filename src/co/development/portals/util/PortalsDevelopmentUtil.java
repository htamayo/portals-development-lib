package co.development.portals.util;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;

import com.liferay.portal.kernel.util.Validator;

/**
 * 
 * @author H. Tamayo
 * @editor H. Tamayo
 * @last edition 12 de oct. de 2016
 *
 */
public class PortalsDevelopmentUtil {

	public static String addLeftZeros(
			String value, Integer totalQuantity) {
		String outputValue = value;
		int zeros  = totalQuantity - value.length();
		//
		if (zeros > 0) {
			for (int i=0; i<zeros; i++) {
				outputValue = '0' + outputValue;
			}
		}
		//
		return outputValue;
	}
	
	public static String formatNumber(
			Double value, Boolean removePrefix) {
		try {
			NumberFormat numberFormat = 
					NumberFormat.getCurrencyInstance(
							Locale.US);
			String formattedValue = numberFormat.format(value);
			//
			if (removePrefix)
				formattedValue = formattedValue.replace(
						"$", "");
			//
			formattedValue = formattedValue.substring(
					0, formattedValue.length());
			//
			return formattedValue;
		} catch (Exception e) {
			return String.valueOf(value);
		}
	}
	
	public static String removeLeftZeros(String value) { 
		return value.replaceAll("^0", "");
	}
	
}
