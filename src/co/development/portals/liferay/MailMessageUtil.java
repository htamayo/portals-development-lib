package co.development.portals.liferay;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.mail.MailMessage;

/**
 * 
 * @author H. Tamayo
 * @last_edition 10/02/2017
 *
 */
public class MailMessageUtil {

	public static MailMessage getMailMessage(
			String subject, String body, Boolean isHTMLFormat, 
			InternetAddress from, InternetAddress...to) {
		MailMessage mailMessage = new MailMessage();
		//
		mailMessage.setSubject(subject);
		mailMessage.setBody(body);
		if (isHTMLFormat != null)
			mailMessage.setHTMLFormat(isHTMLFormat);
		mailMessage.setFrom(from);
		mailMessage.setTo(to);
		//
		return mailMessage;
	}
	
	public static MailMessage getMailMessage(
			String subject, String body, Boolean isHTMLFormat, 
			List<File> files, InternetAddress from, 
			InternetAddress...to) {
		MailMessage mailMessage = getMailMessage(subject, 
				body, isHTMLFormat, from, to);
		//
		for (File file : files) {
			mailMessage.addFileAttachment(file);
		}
		//
		return mailMessage;
	}
	
	public static void sendEmail(String subject, String body, 
			String fromAddress, String toAddress) 
					throws AddressException {
		sendEmail(subject, body, (Boolean)null, fromAddress, 
				toAddress);
	}
	
	public static void sendEmail(String subject, String body, 
			String fromAddress, InternetAddress...to)
					throws AddressException {
		sendEmail(subject, body, (Boolean)null, fromAddress, to);
	}
	
	public static void sendEmail(String subject, String body, 
			String fromAddress, String fromName, String toAddress, 
			String toName) throws UnsupportedEncodingException {
		sendEmail(subject, body, null, fromAddress, fromName, 
				toAddress, toName);
	}
	
	public static void sendEmail(String subject, String body, 
			String fromAddress, String fromName, 
			InternetAddress...to) 
					throws UnsupportedEncodingException {
		sendEmail(subject, body, null, fromAddress, fromName, to);
	}
	
	public static void sendEmail(String subject, String body, 
			InternetAddress from, InternetAddress...to) {
		sendEmail(subject, body, null, from, to);
	}
	
	public static void sendEmail(String subject, String body, 
			Boolean isHTMLFormat, String fromAddress, 
			String toAddress) throws AddressException {
		sendEmail(subject, body, isHTMLFormat, new InternetAddress(
				fromAddress), new InternetAddress(toAddress));
	}
	
	public static void sendEmail(String subject, String body, 
			Boolean isHTMLFormat, String fromAddress, 
			InternetAddress...to) throws AddressException {
		sendEmail(subject, body, isHTMLFormat, new InternetAddress(
				fromAddress), to);
	}
	
	public static void sendEmail(String subject, String body, 
			Boolean isHTMLFormat, String fromAddress, 
			String fromName, String toAddress, String toName) 
					throws UnsupportedEncodingException {
		sendEmail(subject, body, isHTMLFormat, new InternetAddress(
				fromAddress, fromName), new InternetAddress(
						toAddress, toName));
	}
	
	public static void sendEmail(String subject, String body, 
			Boolean isHTMLFormat, String fromAddress, 
			String fromName, InternetAddress...to) 
					throws UnsupportedEncodingException {
		sendEmail(subject, body, isHTMLFormat, new InternetAddress(
				fromAddress, fromName), to);
	}
	
	public static void sendEmail(String subject, String body, 
			Boolean isHTMLFormat, InternetAddress from, 
			List<InternetAddress> to) {
		InternetAddress[] toArray = new InternetAddress[]{};
		sendEmail(subject, body, isHTMLFormat, from, 
				to.toArray(toArray));
	}
	
	public static void sendEmail(String subject, String body, 
			Boolean isHTMLFormat, InternetAddress from, 
			InternetAddress...to) {
		sendEmail(getMailMessage(subject, body, isHTMLFormat, from, 
				to));
	}

	public static void sendEmail(String subject, String body, 
			Boolean isHTMLFormat, List<File> files, 
			InternetAddress from, List<InternetAddress> to) {
		InternetAddress[] toAddress = new InternetAddress[] {};
		sendEmail(subject, body, isHTMLFormat, files, from, 
				to.toArray(toAddress));
	}
	
	public static void sendEmail(String subject, String body, 
			Boolean isHTMLFormat, List<File> files, 
			InternetAddress from, InternetAddress...to) {
		sendEmail(getMailMessage(subject, body, isHTMLFormat, files, 
				from, to));
	}
	
	public static void sendEmail(MailMessage mailMessage) {
		MailServiceUtil.sendEmail(mailMessage);
	}
	
}
