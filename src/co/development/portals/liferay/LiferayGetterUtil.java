package co.development.portals.liferay;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;

import co.development.portals.types.annotation.MappingExclude;
import co.development.portals.types.ObjectMapper;

/**
 * 
 * @author H. Tamayo 
 * @last_edition 25/05/2019
 *
 */
public class LiferayGetterUtil {
	//
	public static <T> T copyRequestObjectToOtherRequest(
			PortletRequest portletRequest, Class<T> clazz)
		throws IllegalAccessException, InstantiationException, 
		NoSuchMethodException, SecurityException, 
		IllegalArgumentException, InvocationTargetException {
		T object = getRequestObject(portletRequest, clazz);
		//
		setRequestObject(portletRequest, object);
		//
		return object;
	}
	//
	public static <T> T getRequestObject(
			PortletRequest portletRequest, Class<T> clazz) 
					throws IllegalAccessException, 
					InstantiationException, NoSuchMethodException, 
					SecurityException, IllegalArgumentException, 
					InvocationTargetException {
		T object = clazz.newInstance();
		//
		Field[] fields = clazz.getDeclaredFields();
		//
		if (fields == null) {
			fields = new Field[]{};
		}
		//
		for (Field field : fields) {
			field.setAccessible(true);
			//
			if (!field.isAnnotationPresent(MappingExclude.class)) { 
				if (ObjectMapper.isAcceptableField(field)) {
					Class<?> fieldType = field.getType();
					//
					Method method = ParamUtil.class.getDeclaredMethod(
							PARAM_METHOD_BY_CLASS.get(fieldType), 
							PortletRequest.class, String.class);
					Object value = method.invoke(null, portletRequest, 
							field.getName());
					//
					field.set(object, value);
				}
			}
		}
		return object;
	}
	//
	public static <T> void setResponseParams(
			ActionResponse actionResponse, T object) throws IllegalAccessException, 
					InstantiationException {
		Field[] fields = object.getClass().getDeclaredFields();
		//
		if (fields == null) {
			fields = new Field[]{};
		}
		//
		for (Field field : fields) {
			field.setAccessible(true);
			// 
			if (!field.isAnnotationPresent(
					MappingExclude.class)) { 
				if (ObjectMapper.isAcceptableField(field)) {
					String fieldName = field.getName();
					Object value = field.get(object);
					//
					actionResponse.setRenderParameter(
							fieldName, String.valueOf(value));
				}
			}
		}
	}
	//
	public static <T> void setRequestObject(
			PortletRequest portletRequest, T object) throws IllegalAccessException, 
					InstantiationException {
		Field[] fields = object.getClass().getDeclaredFields();
		//
		if (fields == null) {
			fields = new Field[]{};
		}
		//
		for (Field field : fields) {
			field.setAccessible(true);
			// 
			if (!field.isAnnotationPresent(MappingExclude.class)) { 
				if (ObjectMapper.isAcceptableField(field)) {
					String fieldName = field.getName();
					Object value = field.get(object);
					//
					portletRequest.setAttribute(fieldName, value);
				}
			}
		}
	}
	//
	public static Map<Class<?>, String> PARAM_METHOD_BY_CLASS;
	static {
		Map<Class<?>, String> map = new HashMap<Class<?>, String>();
		//
		for (Class<?> clazz : ObjectMapper.ACCEPTED_CLASSES) {
			Class<?> finalClazz = null;
			if (clazz.isPrimitive()) 
				finalClazz = ObjectMapper.wrapPrimitive(clazz);
			else 
				finalClazz = clazz;
			// 
			String simpleName = StringPool.BLANK;
			simpleName = finalClazz.getSimpleName();
			//
			map.put(clazz, "get" + simpleName);
		}
		//
		PARAM_METHOD_BY_CLASS = Collections.unmodifiableMap(map);
	}
}
