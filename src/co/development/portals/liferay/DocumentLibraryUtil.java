package co.development.portals.liferay;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.portlet.PortletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.poi.POIDocument;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.documentlibrary.store.DLStoreUtil;

/**
 * 
 * @author H. Tamayo 
 * @last_edition 28/02/2019
 *
 */
public class DocumentLibraryUtil {
	
	/**
	 * 
	 * @param inputStream
	 * @param outputStream
	 * @return integer that represents the number of bytes copied.
	 * @throws IOException
	 */
	public static int copyInputToOutput(
			InputStream inputStream, OutputStream outputStream)
		throws IOException {
		return IOUtils.copy(inputStream, outputStream);
	}
	
	public static DLFileEntry createDLFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName) throws FileNotFoundException, 
					PortalException, SystemException {
		return createDLFileEntry(portletRequest, file, 
				folderName, StringPool.BLANK);
	}
	
	public static DLFileEntry createDLFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName, String fileDescription) 
					throws FileNotFoundException, 
					PortalException, SystemException {
		return createDLFileEntry(portletRequest, file, 
				folderName, fileDescription, 
				"This file is added via programatically");
	}
	
	public static DLFileEntry createDLFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName, String fileDescription, 
			String log) throws FileNotFoundException, 
					PortalException, SystemException {
		return createDLFileEntry(portletRequest, file, 
				folderName, fileDescription, log, 
				"Draft to save");
	}
	
	public static DLFileEntry createDLFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName, String fileDescription, 
			String log, String draftLog) 
					throws FileNotFoundException, 
					PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) 
				portletRequest.getAttribute(
						WebKeys.THEME_DISPLAY);
		//
		return createDLFileEntry(portletRequest, file, 
				themeDisplay, folderName, fileDescription, log, 
				draftLog);
	}
	
	public static DLFileEntry createDLFileEntry(
			PortletRequest portletRequest, File file, 
			ThemeDisplay themeDisplay, String folderName, 
			String fileDescription, String log, String draftLog) 
					throws FileNotFoundException, 
					PortalException, SystemException {
		return createDLFileEntry(portletRequest, file, 
				themeDisplay, folderName, fileDescription, log, 
				draftLog, false);
	}
	
	public static DLFileEntry createDLFileEntry(
			PortletRequest portletRequest, File file, 
			ThemeDisplay themeDisplay, String folderName, 
			String fileDescription, String log, String draftLog, 
			boolean inParent) 
					throws FileNotFoundException, PortalException, 
					SystemException {
		String fileName = file.getName();
		//
		return createDLFileEntry(portletRequest, file, themeDisplay, 
				folderName, fileDescription, log, draftLog, 
				inParent, fileName);
	}
	
	public static DLFileEntry createDLFileEntry(
			PortletRequest portletRequest, File file, 
			ThemeDisplay themeDisplay, String folderName, 
			String fileDescription, String log, String draftLog, 
			boolean inParent, String fileName) 
					throws FileNotFoundException, 
					PortalException, SystemException {
		long parentFolderId = 
				DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, 
				userId = themeDisplay.getUserId(), 
				groupId = ThemeDisplayUtil.getGroupId(
						themeDisplay, inParent), 
				repositoryId = groupId;
		//
		return createDLFileEntry(portletRequest, file, 
				folderName, parentFolderId, userId, groupId, 
				repositoryId, fileDescription, log, draftLog, 
				fileName);
	}
	
	public static DLFileEntry createDLFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName, long parentFolderId, long userId, 
			long groupId, long repositoryId, 
			String fileDescription, String log, String draftLog, 
			String fileName) 
					throws FileNotFoundException, 
					PortalException, SystemException {
		DLFolder dlFolder = DLFolderLocalServiceUtil.getFolder(
				groupId, parentFolderId, folderName);
		ServiceContext serviceContext = 
				ServiceContextFactory.getInstance(
						DLFileEntry.class.getName(), 
						portletRequest);
		//
		long folderId = dlFolder.getFolderId();
		long fileEntryTypeId = 
				dlFolder.getDefaultFileEntryTypeId();
		long fileSize = file.length();
		String mimeType = MimeTypesUtil.getContentType(file);
		//
		InputStream inputStream = new FileInputStream(file);
		DLFileEntry dlFileEntry = 
				DLFileEntryLocalServiceUtil.addFileEntry(
						userId, groupId, repositoryId, folderId, 
						fileName, mimeType, fileName, 
						fileDescription, log, fileEntryTypeId, 
						null, file, inputStream, fileSize,  
						serviceContext);
		//
		long fileEntryId = dlFileEntry.getFileEntryId();
		//
		return DLFileEntryLocalServiceUtil.updateFileEntry(userId, 
				fileEntryId, fileName, mimeType, fileName, 
				fileDescription, draftLog, true, 
				fileEntryTypeId, null, file, null, fileSize, 
				serviceContext);
	}
	
	public static DLFolder createDLFolder(PortletRequest portletRequest, 
			String name, String description) 
					throws PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) 
				portletRequest.getAttribute(
						WebKeys.THEME_DISPLAY);
		return createDLFolder(portletRequest, themeDisplay, 
				name, description);
	}
	
	public static DLFolder createDLFolder(PortletRequest portletRequest, 
			ThemeDisplay themeDisplay, String name, 
			String description) throws PortalException, 
					SystemException {
		return createDLFolder(portletRequest, themeDisplay, 
				name, description, false);
	}
	
	public static DLFolder createDLFolder(PortletRequest portletRequest, 
			ThemeDisplay themeDisplay, String name, 
			String description, boolean inParent) 
					throws PortalException, SystemException {
		long userId = themeDisplay.getUserId(), 
				groupId = ThemeDisplayUtil.getGroupId(
						themeDisplay, inParent), 
				repositoryId = groupId;
		//
		return createDLFolder(portletRequest, name, description, 
				userId, groupId, repositoryId, false, 
				DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, 
				false);
	}
	
	public static DLFolder createDLFolder(PortletRequest portletRequest, 
			String name, String description,	
			long userId, long groupId, long repositoryId, 
			boolean mountPoint, long parentFolderId, 
			boolean hidden) throws PortalException, 
					SystemException {
		ServiceContext serviceContext = 
				ServiceContextFactory.getInstance(
						DLFolder.class.getName(), 
						portletRequest);
		return DLFolderLocalServiceUtil.addFolder(userId, groupId, 
				repositoryId, mountPoint, parentFolderId, 
				name, description, serviceContext);
	}
	
	public static FileEntry createFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName) throws FileNotFoundException, 
					PortalException, SystemException {
		return createFileEntry(portletRequest, file, 
				folderName, StringPool.BLANK);
	}
	
	public static FileEntry createFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName, String fileDescription) 
					throws FileNotFoundException, 
					PortalException, SystemException {
		return createFileEntry(portletRequest, file, 
				folderName, fileDescription, 
				"This file is added via programatically");
	}
	
	public static FileEntry createFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName, String fileDescription, 
			String log) throws FileNotFoundException, 
					PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) 
				portletRequest.getAttribute(
						WebKeys.THEME_DISPLAY);
		//
		return createFileEntry(portletRequest, file, 
				themeDisplay, folderName, fileDescription, log);
	}
	
	public static FileEntry createFileEntry(
			PortletRequest portletRequest, File file, 
			ThemeDisplay themeDisplay, String folderName, 
			String fileDescription, String log) 
					throws FileNotFoundException, 
					PortalException, SystemException {
		return createFileEntry(portletRequest, file, 
				themeDisplay, folderName, fileDescription, log, 
				false);
	}
	
	public static FileEntry createFileEntry(
			PortletRequest portletRequest, File file, 
			ThemeDisplay themeDisplay, String folderName, 
			String fileDescription, String log, 
			boolean inParent) throws FileNotFoundException, 
					PortalException, SystemException {
		long groupId = ThemeDisplayUtil.getGroupId(themeDisplay, 
				inParent), 
				repositoryId = groupId, 
				parentFolderId = 
				DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		//
		return createFileEntry(portletRequest, file, folderName, 
				groupId, repositoryId, parentFolderId, 
				fileDescription, log);
	}
	
	public static FileEntry createFileEntry(
			PortletRequest portletRequest, File file, 
			String folderName, long groupId, long repositoryId, 
			long parentFolderId, String fileDescription, 
			String log) throws FileNotFoundException, 
					PortalException, SystemException {
		Folder folder = DLAppServiceUtil.getFolder(groupId, 
				parentFolderId, folderName);
		ServiceContext serviceContext = 
				ServiceContextFactory.getInstance(
						DLFileEntry.class.getName(), 
						portletRequest);
		//
		long folderId = folder.getFolderId(), 
				size = file.getTotalSpace(); 
		String fileName = file.getName(), 
				mimeType = MimeTypesUtil.getContentType(file);
		//
		InputStream inputStream = new FileInputStream(file);
		return DLAppServiceUtil.addFileEntry(repositoryId, folderId, 
				fileName, mimeType, fileName, fileDescription, 
				log, inputStream, size, serviceContext);
	}
	
	public static Folder createFolder(PortletRequest portletRequest, 
			String name, String description) 
					throws PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) 
				portletRequest.getAttribute(
						WebKeys.THEME_DISPLAY);
		return createFolder(portletRequest, themeDisplay, 
				name, description);
	}
	
	public static Folder createFolder(PortletRequest portletRequest, 
			ThemeDisplay themeDisplay, String name, 
			String description) throws PortalException, 
				SystemException {
		return createFolder(portletRequest, themeDisplay, name, 
				description, false);
	}
	
	public static Folder createFolder(PortletRequest portletRequest, 
			ThemeDisplay themeDisplay, String name, 
			String description, boolean inParent) 
					throws PortalException, SystemException {
		long repositoryId = ThemeDisplayUtil.getGroupId(
				themeDisplay, inParent), 
				parentFolderId = 
				DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		//
		return createFolder(portletRequest, name, description, 
				repositoryId, parentFolderId);
	}
	
	public static Folder createFolder(PortletRequest portletRequest, 
			String name, String description, long repositoryId, 
			long parentFolderId) throws PortalException, 
					SystemException {
		ServiceContext serviceContext = 
				ServiceContextFactory.getInstance(
						DLFolder.class.getName(), 
						portletRequest);
		return DLAppServiceUtil.addFolder(repositoryId, 
				parentFolderId, name, description, 
				serviceContext);
	}
	
	public static PdfObjects getPdfObjects(
			PortletRequest portletRequest, String folderName, 
			String fileName, String newFileName, 
			String fileExtension) throws DocumentException, 
					IOException, PortalException, 
					SystemException {
		return getPdfObjects(portletRequest, folderName, 
				fileName, newFileName, fileExtension, true);
	}
	
	/**
	 * This method will create several Objects:
	 * PdfReader, File, FileOutputStream, 
	 * Opened Document, PdfWrtirer and PdfContentByte.
	 * 
	 * @param portletRequest
	 * @param folderName
	 * @param fileName
	 * @param newFileName
	 * @param fileExtension
	 * @param inParent
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static PdfObjects getPdfObjects(
			PortletRequest portletRequest, String folderName, 
			String fileName, String newFileName, 
			String fileExtension, boolean inParent)
					throws DocumentException, IOException, 
					PortalException, SystemException {
		boolean createFile = true, createPdfStamper = false, 
				createDocumentObjects = true, importPage = true;
		//
		return getPdfObjects(portletRequest, folderName, 
				fileName, newFileName, fileExtension, 
				inParent, createFile, createPdfStamper, 
				createDocumentObjects, importPage);
	}
	
	public static PdfObjects getPdfObjects(
			PortletRequest portletRequest, String folderName, 
			String fileName, String newFileName, 
			String newFileExtension, boolean inParent, 
			boolean createFile, boolean createPdfStamper, 
			boolean createDocumentObjects, boolean importPage) 
					throws DocumentException, IOException, 
					PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) 
				portletRequest.getAttribute(
						WebKeys.THEME_DISPLAY);
		//
		long userId = themeDisplay.getUserId(), 
				groupId = ThemeDisplayUtil.getGroupId(themeDisplay, 
						inParent);
		//
		PdfObjects pdfObjects = 
				new PdfObjects();
		//
		pdfObjects.setInputStream(getFileInputStream(
				userId, groupId, folderName, fileName));
		pdfObjects.createPdfReader(createFile, 
				newFileName, newFileExtension, createPdfStamper, 
				createDocumentObjects, importPage);
		//
		return pdfObjects;
	}
	
	public static PdfObjects getPdfObjects(
			PortletRequest portletRequest, String folderName, 
			String fileName) throws DocumentException, 
					IOException, PortalException, 
					SystemException {
		return getPdfObjects(portletRequest, folderName, 
				fileName, false);
	}
	
	public static PdfObjects getPdfObjects(
			PortletRequest portletRequest, String folderName, 
			String fileName, boolean inParent) 
					throws DocumentException, IOException, 
					PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) 
				portletRequest.getAttribute(
						WebKeys.THEME_DISPLAY);
		//
		return getPdfObjects(themeDisplay, folderName, 
				fileName, inParent);
	}
	
	public static PdfObjects getPdfObjects(
			ThemeDisplay themeDisplay, String folderName, 
			String fileName) throws DocumentException, 
					IOException, PortalException, 
					SystemException {
		return getPdfObjects(themeDisplay, 
				folderName, fileName, false);
	}
	
	public static PdfObjects getPdfObjects(
			ThemeDisplay themeDisplay, String folderName, 
			String fileName, boolean inParent) 
					throws DocumentException, IOException, 
					PortalException, SystemException {
		long userId = themeDisplay.getUserId(), 
				groupId = ThemeDisplayUtil.getGroupId(
						themeDisplay, inParent);
		//
		return getPdfObjects(userId, groupId, 
				folderName, fileName);
	}
	
	public static PdfObjects getPdfObjects(
			long userId, long groupId, String folderName, 
			String fileName) throws DocumentException, IOException, 
					PortalException, SystemException {
		PdfObjects pdfObjects = 
				new PdfObjects();
		//
		pdfObjects.setInputStream(getFileInputStream(
				userId, groupId, folderName, fileName));
		pdfObjects.createPdfReader();
		//
		return pdfObjects;
	}
	
	public static POIDocumentWithInputStream 
		getWorkBookWithInputStream(long userId, long groupId, 
				String folderName, String fileName)
		throws IOException, PortalException, SystemException {
		POIDocumentWithInputStream pOIDocumentWithInputStream = 
				new POIDocumentWithInputStream();
		//
		InputStream inputStream = getFileInputStream(userId, 
				groupId, folderName, fileName);
		//
		HSSFWorkbook workBook = new HSSFWorkbook(inputStream);
		//
		pOIDocumentWithInputStream.setPOIDocument(workBook);
		pOIDocumentWithInputStream.setInputStream(inputStream);
		//
		return pOIDocumentWithInputStream;
	}
	
	public static HSSFWorkbook getDLFileEntryAsWorkBook(
			long userId, long groupId, String folderName, 
			String fileName)
		throws IOException, PortalException, SystemException {
		InputStream inputStream = getFileInputStream(userId, 
				groupId, folderName, fileName);
		//
		return new HSSFWorkbook(inputStream);
	}
	
	public static InputStream getFileInputStream(long userId, 
			long groupId, String folderName, String fileName) 
		throws PortalException, SystemException {
		DLFileEntry dlFileEntry = getDLFileEntry(groupId, 
				folderName, fileName);
		//
		return DLFileEntryLocalServiceUtil.getFileAsStream(
				userId, dlFileEntry.getFileEntryId(), 
				dlFileEntry.getVersion());
	}	
	
	public static File getFile(long userId, long groupId, 
			String folderName, String fileName) 
					throws PortalException, SystemException {
		return getFile(userId, groupId, StringPool.BLANK, 
				folderName, fileName);
	}
	
	public static File getFile(long userId, long groupId, 
			String parentFolderName, String folderName, 
			String fileName) throws PortalException, 
					SystemException {
		DLFileEntry dlFileEntry = getDLFileEntry(groupId, 
			parentFolderName, folderName, fileName);
		
		//
		return DLFileEntryLocalServiceUtil.getFile(userId, 
			dlFileEntry.getFileEntryId(), dlFileEntry.getVersion(), 
			true);
	}
	
	public static String getDLFileDownloadURL(
			ThemeDisplay themeDisplay, long groupId, 
			String folderName, String fileName) 
					throws PortalException, SystemException { 
		return getDLFileDownloadURL(themeDisplay, groupId, 
				folderName, fileName, false, StringPool.BLANK);
	}
	
	public static String getDLFileDownloadURL(
			ThemeDisplay themeDisplay, long groupId, 
			String folderName, String fileName, 
			boolean validateExist, String defaultFile) 
					throws PortalException, SystemException { 
		return getDLFileDownloadURL(themeDisplay, groupId, 
				StringPool.BLANK, folderName, fileName, 
				validateExist, defaultFile);
	}
	
	public static String getDLFileDownloadURL(
			ThemeDisplay themeDisplay, long groupId, 
			String parentFolderName, String folderName, 
			String fileName) 
					throws PortalException, SystemException {
		return getDLFileDownloadURL(themeDisplay, groupId, 
				parentFolderName, folderName, fileName, 
				false, StringPool.BLANK);
	}
	//
	public static String getDLFileDownloadURL(
			ThemeDisplay themeDisplay, long groupId, 
			String parentFolderName, String folderName, 
			String fileName, boolean validateExist, 
			String defaultFile) 
					throws PortalException, SystemException {
		DLFileEntry dlFileEntry = getDLFileEntry(groupId, 
				parentFolderName, folderName, fileName);
		//
		boolean exist = true;
		if (validateExist) {
			if (Validator.isNotNull(dlFileEntry)) {
				File file = getFile(groupId, parentFolderName, 
						folderName, fileName);
				//
				if (file == null) {
					exist = false;
				}
			} else {
				exist = false;
			}
		}
		//
		return getDLFileDownloadURL(themeDisplay, groupId, 
				dlFileEntry, exist, defaultFile);
	}
	//
	public static String getDLFileDownloadURL(
			ThemeDisplay themeDisplay, long groupId, 
			DLFileEntry dlFileEntry) {
		return getDLFileDownloadURL(themeDisplay, groupId, 
				dlFileEntry, true);
	}
	//
	public static String getDLFileDownloadURL(
			ThemeDisplay themeDisplay, long groupId, 
			DLFileEntry dlFileEntry, boolean exists) {
		return getDLFileDownloadURL(themeDisplay, groupId, 
				dlFileEntry, exists, StringPool.BLANK);
	}
	//
	public static String getDLFileDownloadURL(
			ThemeDisplay themeDisplay, long groupId, 
			DLFileEntry dlFileEntry, boolean exist, 
			String defaultFile) {
		//
		String url = StringPool.BLANK;
		url = themeDisplay.getPortalURL() + DOCUMENTS_ROOT 
				+ groupId + StringPool.FORWARD_SLASH 
				+ dlFileEntry.getFolderId() 
				+ StringPool.FORWARD_SLASH;
		//
		if (Validator.isNotNull(dlFileEntry.getFileEntryId()) || 
				!dlFileEntry.getTitle().startsWith(
				StringPool.FORWARD_SLASH)) {
			if (exist) {
				url += dlFileEntry.getTitle();
			} else if (!defaultFile.equals(StringPool.BLANK)){
				url += defaultFile;
			} else {
				url += StringPool.BLANK;
			}
		} else {
			if (exist) {
				url = StringPool.BLANK;
			} else if (!defaultFile.equals(StringPool.BLANK)) {
				url += defaultFile;
			} else {
				url += StringPool.BLANK;
			}
			
		}
		//
		return url;
	}
	//
	public static File getFile(long groupId, String folderName, 
			String fileName) throws PortalException, 
					SystemException {
		return getFile(groupId, StringPool.BLANK, folderName, 
				fileName);
	}
	//
	public static File getFile(long groupId, 
			String parentFolderName, String folderName, 
			String fileName) throws PortalException, 
					SystemException {
		DLFileEntry dlFileEntry = getDLFileEntry(groupId, 
				parentFolderName, folderName, fileName);
		//
		return getFile(dlFileEntry);
	}
	//
	public static File getFile(DLFileEntry dlFileEntry) 
			throws PortalException, SystemException {
		if (Validator.isNotNull(dlFileEntry.getFileEntryId())) {
			return DLStoreUtil.getFile(dlFileEntry.getCompanyId(), 
					dlFileEntry.getFolderId(), 
					dlFileEntry.getName());
		} else {
			return null;
		}
	}
	//
	public static DLFileEntry getDLFileEntry(long groupId, 
			String folderName, String fileName)
		throws PortalException, SystemException {
		return getDLFileEntry(groupId, StringPool.BLANK, folderName, 
				fileName);
	}
	
	public static DLFileEntry getDLFileEntry(long groupId, 
			String parentFolderName, String folderName, 
			String fileName) throws PortalException, 
					SystemException {
		long parentFolderId = DEFAULT_PARENT_FOLDER_ID;
		if (Validator.isNotNull(parentFolderName)) {
			DLFolder dlParentFolder = 
					DLFolderLocalServiceUtil.getFolder(
							groupId, DEFAULT_PARENT_FOLDER_ID, 
							parentFolderName);
			//
			parentFolderId = dlParentFolder.getFolderId();
		}
		//
		DLFolder dlFolder = DLFolderLocalServiceUtil.getFolder(
				groupId, parentFolderId, folderName);
		try {
			DLFileEntry dlFileEntry = 
					DLFileEntryLocalServiceUtil.getFileEntry(
							groupId, dlFolder.getFolderId(),
							fileName);
			return dlFileEntry;
		} catch (Exception ex) {
			if (_log.isDebugEnabled()) {
				_log.error(ex.getMessage(), ex);
			} else {
				_log.error(ex.getMessage());
			}
			//
			DLFileEntry dlFileEntry = 
					DLFileEntryLocalServiceUtil.createDLFileEntry(
							0);
			dlFileEntry.setFolderId(dlFolder.getFolderId());
			//
			return dlFileEntry;
		}
	}
	
	public static File copyFileToNewOne(File source, 
			String newFileName, String fileExtension) 
		throws IOException {
		File destination = File.createTempFile(
				newFileName, fileExtension);
		//
		FileUtil.copyFile(source, destination);
		//
		return destination;
	}
	
	/**
	 * 
	 * @author H. Tamayo
	 * @editor H. Tamayo
	 * @last_edition 02/03/2017
	 *
	 */
	public static class PdfObjects 
			extends InputStreamModel {
		
		/**
		 * 
		 * @author H. Tamayo 
		 * @last_edition 26/02/2019
		 *
		 */
		public static class Footer extends PdfPageEventHelper {
			//
			private Font font;
			private String footer; 
			private PdfTemplate total;
			//
			public void setFooter(String footer) {
				this.footer = footer;
			}
			//
			@Override
			public void onOpenDocument(PdfWriter writer, 
					Document document) {
				total = writer.getDirectContent().createTemplate(
						30, 16);
				//
				try {
					font = new Font(BaseFont.createFont(
							BaseFont.HELVETICA, 
							BaseFont.CP1252, 
							BaseFont.NOT_EMBEDDED), 10);
				} catch (DocumentException de) {
					throw new ExceptionConverter(de);
				} catch (IOException ioe) {
					throw new ExceptionConverter(ioe);
				}
			}
			//
			@Override
			public void onEndPage(PdfWriter pdfWriter, 
					Document document) {
				PdfPTable pdfTable = new PdfPTable(3);
				try {
					pdfTable.setWidths(new int[]{24, 24, 2});
					pdfTable.setTotalWidth(527);
					pdfTable.setLockedWidth(true);
					pdfTable.getDefaultCell().setFixedHeight(20);
					pdfTable.getDefaultCell().setBorder(Rectangle.BOTTOM);
					//
					pdfTable.addCell(new Phrase(footer, font));
					pdfTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfTable.addCell(new Phrase(String.format("P�g %d of", pdfWriter.getPageNumber()), font));
					PdfPCell cell = new PdfPCell(Image.getInstance(total));
					cell.setBorder(Rectangle.BOTTOM);
					pdfTable.addCell(cell);
	                pdfTable.writeSelectedRows(0, -1, 36, 30, pdfWriter.getDirectContent());
				} catch (DocumentException de) {
					throw new ExceptionConverter(de);
				}
			}
			//
			@Override
			public void onCloseDocument(PdfWriter pdfWriter, 
					Document document) {
				ColumnText.showTextAligned(total, 
						Element.ALIGN_LEFT, 
						new Phrase(String.valueOf(
								pdfWriter.getPageNumber()), font), 
						2, 4, 0);
			}
		}
		//
		/**
		 * 
		 * @author H. Tamayo
		 * @last_edition 16/02/2017
		 *
		 */
		public class SameTemplate extends PdfPageEventHelper {
			protected PdfReader pdfReader;
			
			public SameTemplate() {
				this(null);
			}
			
			public SameTemplate(PdfReader pdfReader) {
				this.pdfReader = pdfReader;
			}
			
			@Override
			public void onStartPage(PdfWriter writer, 
					Document document) {
				
				if (this.pdfReader != null) {
					PdfImportedPage pdfImportedPage = 
							writer.getImportedPage(
									this.pdfReader, 1);
					//
					PdfContentByte pdfContentByte = 
							writer.getDirectContent();
					pdfContentByte.addTemplate(pdfImportedPage, 
							0, 0);
				}
				//
				//super.onEndPage(writer, document);
			}
		}
		
		public void closePdfObjects(boolean closeDocument) {
			if (closeDocument) 
				this.getDocument().close();
			// this.file
			try {
				if (this.getInputStream() != null)
					this.getInputStream().close();
			} catch (IOException ex) {
				_log.error(ex.getMessage());
			}
			//
			try {
				if (this.getOutputStream() != null)
					this.getOutputStream().close();
			} catch (IOException ex) {
				_log.error(ex.getMessage(), ex);
			}
			//
			if (this.getPdfWriter() != null)
				this.getPdfWriter().close();
			//
			if (this.getPdfReader() != null) 
				this.getPdfReader().close();
			//
			try {
			if (this.getPdfStamper() != null)
				this.pdfStamper.close();
			} catch (Exception ex) {
				_log.error(ex.getMessage(), ex);
			}
		}
		
		public void createByteArrayOutputStream(
				boolean createPdfStamper, 
				boolean createDocument, boolean importPage) 
						throws DocumentException, IOException {
			this.outputStream = new ByteArrayOutputStream();
			//
			if (createPdfStamper)
				createPdfStamper(false, importPage);
			//
			if (createDocument)
				createDocumentObjects(false, PageSize.LETTER, 
						importPage);
		}
		
		/**
		 * 
		 * @param createFileOutputStream
		 * @param pageSize
		 * @param importPage 
		 * @throws DocumentException
		 * @throws IOException
		 */
		public void createDocumentObjects(
				boolean isOutputFile, 
				Rectangle pageSize, boolean importPage) 
						throws DocumentException, IOException {
			if (isOutputFile)
				createFileOutputStream();
			//
			if (this.outputStream == null)
				throw new IOException("The OutputStream " + 
						"Object is null");
			//
			this.document = new Document(pageSize);
			this.pdfWriter = PdfWriter.getInstance(
					this.document, this.outputStream);
			//
			if (importPage) {
				this.pdfWriter.setPageEvent(
						new SameTemplate(this.pdfReader));
			}
		}
		
		public void createFile(String fileName, 
				String fileExtension, boolean createPdfStamper, 
				boolean createDocument, boolean importPage) 
						throws DocumentException, IOException {
			if (fileName.equals(StringPool.BLANK))
				throw new IOException("The File Name is Blank");
			//
			if (fileExtension.equals(StringPool.BLANK)) 
				throw new IOException("The File Extension is Blank");
			//
			if (!fileExtension.startsWith(StringPool.PERIOD)) 
				fileExtension = StringPool.PERIOD + fileExtension;
			//
			this.file = File.createTempFile(fileName, fileExtension);
			//
			if (createPdfStamper)
				createPdfStamper(true, importPage);
			//
			if (createDocument)
				createDocumentObjects(true, PageSize.LETTER, 
						importPage);
		}
		
		public void createFileOutputStream() 
				throws IOException {
			if (this.file == null)
				throw new IOException("The File Object is Null");
			//
			this.outputStream = new FileOutputStream(
					this.file);
		}
		
		public void createPdfReader() throws DocumentException, 
				IOException {
			createPdfReader(false, StringPool.BLANK, 
					StringPool.BLANK, false, false, false);
		}
		
		public void createPdfReader(boolean createFile, 
				String fileName, String fileExtension, 
				boolean createPdfStamper, 
				boolean createDocument, boolean importPage) 
						throws DocumentException, IOException {
			InputStream inputStream = this.getInputStream();
			//
			if (inputStream != null) 
				this.pdfReader = new PdfReader(
						this.getInputStream());
			else 
				throw new IOException("The InputStream no has " + 
						"been initalized");
			//
			if (createFile) {
				createFile(fileName, fileExtension, 
						createPdfStamper, createDocument, 
						importPage);
			} else {
				createByteArrayOutputStream(createPdfStamper, 
						createDocument, importPage);
			}
		}
		
		public void createPdfStamper(
				boolean isOutputFile, boolean importPage) 
						throws DocumentException, IOException {
			if (isOutputFile)
				createFileOutputStream();
			//
			if (this.outputStream == null)
				throw new IOException("The FileOutputStream " + 
						"Object is null");
			//
			this.pdfStamper = new PdfStamper(this.pdfReader, 
					this.outputStream);
			//
			if (importPage) {
				this.pdfContentByte = 
						this.pdfStamper.getOverContent(1);
			}
		}
		
		public Document getDocument() {
			return this.document;
		}
		
		public void setDocument(Document document) {
			this.document = document;
		}
		
		public File getFile() {
			return this.file;
		}
		public void setFile(File file) {
			this.file = file;
		}
		public OutputStream getOutputStream() {
			return this.outputStream;
		}
		public void setOutputStream(OutputStream outputStream) {
			this.outputStream = outputStream;
		}
		public PdfContentByte getPdfContentByte() {
			return this.pdfContentByte;
		}
		public void setPdfContentByte(
				PdfContentByte pdfContentByte) {
			this.pdfContentByte = pdfContentByte;
		}
		public PdfWriter getPdfWriter() {
			return this.pdfWriter;
		}
		public void setPdfWriter(PdfWriter pdfWriter) {
			this.pdfWriter = pdfWriter;
		}
		@Override
		public File getInputStreamAsAFile(String fileName, 
				String fileExtension) throws IOException {
			if (file != null) {
				return this.getFile();
			} else {
				throw new IOException("Sorry, File has " + 
						"not been created.");
			}
		}
		public PdfReader getPdfReader() {
			return this.pdfReader;
		}
		public void setPdfReader(PdfReader pdfReader) {
			this.pdfReader = pdfReader;
		}
		public PdfStamper getPdfStamper() {
			return this.pdfStamper;
		}
		public void setPdfStamper(PdfStamper pdfStamper) {
			this.pdfStamper = pdfStamper;
		}
		
		// Result Objects
		private Document document;
		private File file;
		private OutputStream outputStream;
		private PdfWriter pdfWriter;
		// Template Objects
		private PdfReader pdfReader;
		private PdfStamper pdfStamper;
		// Common Objects
		private PdfContentByte pdfContentByte;
	}
	
	/**
	 * 
	 * @author H. Tamayo
	 * @editor H. Tamayo
	 * @last_edition 13/02/2017
	 *
	 */
	public static class POIDocumentWithInputStream 
			extends InputStreamModel {
		
		public POIDocument getPOIDocument() {
			return this.pOIDocument;
		}
		
		public void setPOIDocument(POIDocument pOIDocument) {
			this.pOIDocument = pOIDocument;
		}
		
		@Override
		public File getInputStreamAsAFile(String fileName, 
				String fileExtension) throws IOException {
			this.getInputStream().close();
			//
			if (!fileExtension.startsWith(StringPool.PERIOD))
				fileExtension = StringPool.PERIOD + fileExtension;
			//
			File file = File.createTempFile(fileName, fileExtension);
			/*FileOutputStream fileOutputStream = new 
					FileOutputStream(file);*/
			//
			this.getPOIDocument().write(file);
			//this.getPOIDocument().write(fileOutputStream);
			//
			return file;
		}
		
		private POIDocument pOIDocument;
	}
	
	/**
	 * 
	 * @author H. Tamayo
	 * @editor H. Tamayo
	 * @last_edition 13/02/2017
	 *
	 */
	public static abstract class InputStreamModel {
		
		public InputStream getInputStream() {
			return this.inputStream;
		}
		
		public void setInputStream(InputStream inputStream) {
			this.inputStream = inputStream;
		}
		
		/**
		 * 
		 * @param fileName
		 * @param fileExtension
		 * @return
		 * @throws IOException
		 */
		public abstract File getInputStreamAsAFile(String fileName, 
				String fileExtension) 
						throws IOException;
		
		private InputStream inputStream;
	}

	public static final String DOCUMENTS_ROOT = "/documents/";
	public static final long DEFAULT_PARENT_FOLDER_ID = 0;
	
	private static Log _log = LogFactoryUtil.getLog(
			DocumentLibraryUtil.class);
}
