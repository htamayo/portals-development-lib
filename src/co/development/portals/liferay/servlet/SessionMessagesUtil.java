package co.development.portals.liferay.servlet;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.StringPool;

import co.development.portals.types.BasicPojo;
import co.development.portals.types.StringUtil;

/**
 * 
 * @author H. Tamayo 
 * @last_edition 28/09/2018
 *
 */
public class SessionMessagesUtil {
	// 
	public static List<BasicPojo> getElementErrors(
			PortletRequest portletRequest) {
		return getMessagesAsBasicPojos(portletRequest, 
				ELEMENT_ERROR_ATTRIBUTE_SUFFIX);
	} 
	//
	public static List<BasicPojo> getElementMessages(
			PortletRequest portletRequest) {
		return getMessagesAsBasicPojos(portletRequest, 
				ELEMENT_MESSAGE_ATTRIBUTE_SUFFIX);
	}
	// 
	public static List<BasicPojo> getJournalArticleErrors(
			PortletRequest portletRequest) {
		return getMessagesAsBasicPojos(portletRequest, 
				JA_ATTRIBUTE_SUFFIX);
	} 
	// 
	public static List<BasicPojo> getMessagesAsBasicPojos(
			PortletRequest portletRequest, String attributeSuffix) {
		List<BasicPojo> basicPojos = 
				new ArrayList<BasicPojo>();
		Enumeration<String> attributeNames = 
				portletRequest.getAttributeNames();
		//
		while(attributeNames.hasMoreElements()) {
			String attributeName = attributeNames.nextElement();
			//
			if (attributeName.endsWith(attributeSuffix)) {
				BasicPojo basicPojo = (BasicPojo)
						portletRequest.getAttribute(attributeName);
				//
				if (basicPojo != null)
					basicPojos.add(basicPojo);
			}
		}
		//
		return basicPojos;
	}
	// 
	public static List<BasicPojo> getReflectionCallErrors(
			PortletRequest portletRequest) {
		return getMessagesAsBasicPojos(portletRequest, 
				ERROR_IN_REFLECTION_ATTRIBUTE_SUFFIX); 
	}
	// 
	public static BasicPojo getJournalArticleError(
			String journalArticleName) {
		return getSessionMessage(JA_KEY_PREFIX, JA_KEY_SUFFIX, 
				JA_BODY_PREFIX, JA_BODY_SUFFIX, journalArticleName);
	}
	// 
	public static BasicPojo getSessionMessage(String prefixKey, 
			String suffixKey, String prefixBody, String suffixBody, 
			String elementName) {
		String keyElement = StringPool.BLANK;
		if (!elementName.equals(StringPool.BLANK)) {
			keyElement = 
					StringUtil.convertComposedStringToSeparatedString(
								elementName, StringPool.DASH, false);
		}
		String key = prefixKey + keyElement + suffixKey;
		//
		String bodyElement = StringPool.BLANK;
		if (!elementName.equals(StringPool.BLANK)) {
			bodyElement = 
					StringUtil.convertComposedStringToSeparatedString(
								elementName, StringPool.SPACE, true);
		}
		String body = prefixBody + bodyElement + suffixBody;
		//
		BasicPojo basicPojo = new BasicPojo(key, body);
		//
		return basicPojo;
	}
	//
	public static void setElementError(
			PortletRequest portletRequest, Class<?> clazz) {
		setElementError(portletRequest, clazz.getSimpleName());
	}
	// 
	public static void setElementError(
			PortletRequest portletRequest, String elementName) {
		setElementError(portletRequest, elementName, 
				ELEMENT_ERROR_ATTRIBUTE_SUFFIX, elementName);
	}
	// 
	public static void setElementError(
			PortletRequest portletRequest, String body, 
			String elementName) {
		setElementError(portletRequest, body, elementName,  
				ELEMENT_ERROR_ATTRIBUTE_SUFFIX, elementName);
	}
	// 
	public static void setElementError(String bodyPrefix, 
			String bodySuffix, PortletRequest portletRequest, 
			String elementName) {
		setElementError(portletRequest, bodyPrefix, bodySuffix, 
				elementName, ELEMENT_ERROR_ATTRIBUTE_SUFFIX, 
				elementName);
	}
	// 
	public static void setElementError(
			PortletRequest portletRequest, String attributeName, 
			String attributeSuffix, String elementName) {
		setElementError(portletRequest, ELEMENT_ERROR_BODY_PREFIX, 
				ELEMENT_ERROR_BODY_SUFFIX, attributeName, 
				attributeSuffix, elementName);
	}
	// 
	public static void setElementError(
			PortletRequest portletRequest, String body, 
			String attributeName, String attributeSuffix, 
			String elementName) {
		setElementError(portletRequest, body, StringPool.BLANK, 
				attributeName, attributeSuffix, elementName);
	}
	// 
	public static void setElementError(
			PortletRequest portletRequest, String bodyPrefix, 
			String bodySuffix, String attributeName, 
			String attributeSuffix, String elementName) {
		setSessionMessage(portletRequest, ELEMENT_ERROR_KEY_PREFIX, 
				ELEMENT_ERROR_KEY_SUFFIX, bodyPrefix, 
				bodySuffix, attributeName, attributeSuffix,
				elementName, true);
	}
	// 
	public static void setElementMessage(
			PortletRequest portletRequest, Class<?> clazz) {
		setElementMessage(portletRequest, clazz.getSimpleName());
	}
	// 
	public static void setElementMessage(
			PortletRequest portletRequest, String elementName) {
		setElementMessage(portletRequest, elementName, 
				ELEMENT_MESSAGE_ATTRIBUTE_SUFFIX, elementName);
	}
	// 
	public static void setElementMessage(
			PortletRequest portletRequest, String body, 
			String elementName) {
		setElementMessage(portletRequest, body, elementName, 
				ELEMENT_MESSAGE_ATTRIBUTE_SUFFIX, elementName);
	}
	// 
	public static void setElementMessage(
			String bodyPrefix, 
			String bodySuffix, PortletRequest portletRequest,
			String elementName) {
		setElementMessage(portletRequest, bodyPrefix, bodySuffix, 
				elementName, ELEMENT_MESSAGE_ATTRIBUTE_SUFFIX, 
				elementName);
	}
	// 
	public static void setElementMessage(
			PortletRequest portletRequest, String attributeName, 
			String attributeSuffix, String elementName) {
		setElementMessage(portletRequest, ELEMENT_MESSAGE_BODY_PREFIX, 
				ELEMENT_MESSAGE_BODY_SUFFIX, attributeName, 
				attributeSuffix, elementName);
	}
	// 
	public static void setElementMessage(
			PortletRequest portletRequest, String body, 
			String attributeName, 
			String attributeSuffix, String elementName) {
		setElementMessage(portletRequest, body, StringPool.BLANK, 
				attributeName, attributeSuffix, elementName);
	}
	// 
	public static void setElementMessage(
			PortletRequest portletRequest, String bodyPrefix, 
			String bodySuffix, String attributeName, 
			String attributeSuffix, String elementName) {
		setSessionMessage(portletRequest, ELEMENT_MESSAGE_KEY_PREFIX, 
				ELEMENT_MESSAGE_KEY_SUFFIX, bodyPrefix, 
				bodySuffix, attributeName, attributeSuffix, 
				elementName, false);
	} 
	//
	public static void setJournalArticleError(
			PortletRequest portletRequest, 
			String journalArticleName) {
		setSessionMessage(portletRequest, JA_KEY_PREFIX, 
				JA_KEY_SUFFIX, JA_BODY_PREFIX, JA_BODY_SUFFIX, 
				journalArticleName, JA_ATTRIBUTE_SUFFIX, 
				journalArticleName, false);
	} 
	// 
	public static void setReflectionCallError(
			PortletRequest portletRequest, String elementName) {
		setReflectionCallError(portletRequest, elementName, 
				ERROR_IN_REFLECTION_ATTRIBUTE_SUFFIX, elementName); 
	}
	//
	public static void setReflectionCallError(String bodyPrefix, 
			String bodySuffix, PortletRequest portletRequest, 
			String elementName) {
		setReflectionCallError(portletRequest, bodyPrefix, bodySuffix, 
				elementName, ERROR_IN_REFLECTION_ATTRIBUTE_SUFFIX, 
				elementName); 
	}
	// 
	public static void setReflectionCallError(
			PortletRequest portletRequest, String attributeName, 
			String attributeSuffix, String elementName) {
		setReflectionCallError(portletRequest, 
				ERROR_IN_REFLECTION_CALL_BODY_PREFFIX, 
				ERROR_IN_REFLECTION_CALL_BODY_SUFFIX, attributeName, 
				attributeSuffix, elementName); 
	}
	//
	public static void setReflectionCallError(
			PortletRequest portletRequest, String body, 
			String attributeName, String attributeSuffix, 
			String elementName) {
		setReflectionCallError(portletRequest, body, 
				StringPool.BLANK, attributeName, attributeSuffix, 
				elementName); 
	}
	// 
	public static void setReflectionCallError(
			PortletRequest portletRequest, String bodyPrefix, 
			String bodySuffix, String attributeName, 
			String attributeSuffix, String elementName) {
		setSessionMessage(portletRequest, 
				ERROR_IN_REFLECTION_CALL_KEY_PREFIX, 
				ERROR_IN_REFLECTION_CALL_KEY_SUFFIX, bodyPrefix, 
				bodySuffix, attributeName, attributeSuffix, 
				elementName, true); 
	}
	// 
	public static void setSessionMessage(
			PortletRequest portletRequest, 
			String prefixKey, String suffixKey, String prefixBody, 
			String suffixBody, String attributeName, 
			String attributeSuffix, String elementName, 
			boolean isError) {
		BasicPojo basicPojo = getSessionMessage(prefixKey, 
				suffixKey, prefixBody, suffixBody, elementName);
		//
		portletRequest.setAttribute(attributeName + 
				attributeSuffix, basicPojo);
		//
		if (isError)
			SessionErrors.add(portletRequest, basicPojo.getKey());
		else 
			SessionMessages.add(portletRequest, 
					basicPojo.getKey());
	}
	// 
	public static final String ELEMENT_ERROR_KEY_PREFIX = 
			"error-in-processing-for-";
	public static final String ELEMENT_ERROR_KEY_SUFFIX = 
			"-element";
	public static final String ELEMENT_ERROR_BODY_PREFIX = 
			"Error en el procesamiento del objeto: ";
	public static final String ELEMENT_ERROR_BODY_SUFFIX = 
			". Comun�quese con el Administrador";
	public static final String ELEMENT_ERROR_ATTRIBUTE_SUFFIX = 
			"_ELEMENT_PROCESSING_ERROR";
	public static final String ELEMENT_MESSAGE_KEY_PREFIX = 
			"processing-for-the-";
	public static final String ELEMENT_MESSAGE_KEY_SUFFIX = 
			"-was-successful";
	public static final String ELEMENT_MESSAGE_BODY_PREFIX = 
			"El procesamiento para el ";
	public static final String ELEMENT_MESSAGE_BODY_SUFFIX = 
			" fu� Satisfactorio.";
	public static final String ELEMENT_MESSAGE_ATTRIBUTE_SUFFIX = 
			"_ELEMENT_PROCESSING_MESSAGE";
	public static final String JA_KEY_PREFIX = 
			"error-in-retrieving-for-";
	public static final String JA_KEY_SUFFIX = "-journal-article";
	public static final String JA_BODY_PREFIX = 
			"Error en la recuperaci�n del Contenido Web: ";
	public static final String JA_BODY_SUFFIX = 
			". Comun�quese con el Administrador.";
	public static final String JA_ATTRIBUTE_SUFFIX = 
			"_JOURNAL_ARTICLE_CALL_AND_SETTING_ERROR"; 
	public static final String ERROR_IN_REFLECTION_CALL_KEY_PREFIX = 
			"error-in-reflection-call-for-";
	public static final String ERROR_IN_REFLECTION_CALL_KEY_SUFFIX = 
			"-method";
	public static final String ERROR_IN_REFLECTION_CALL_BODY_PREFFIX = 
			"Error en el llamado a trav�s de Reflection para el "
			+ "m�todo: ";
	public static final String ERROR_IN_REFLECTION_CALL_BODY_SUFFIX = 
			". Comun�quese con el Administrador";
	public static final String ERROR_IN_REFLECTION_ATTRIBUTE_SUFFIX = 
			"_REFLECTION_CALL_ERROR";
	
}
