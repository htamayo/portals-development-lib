package co.development.portals.liferay;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * 
 * @author H. Tamayo
 * @editor H. Tamayo
 * @last_edition 13/02/2017
 *
 */
public class ThemeDisplayUtil {

	public static long getGroupId(PortletRequest portletRequest) {
		return getGroupId(portletRequest, false);
	}
	
	public static long getGroupId(PortletRequest portletRequest, 
			boolean inParent) {
		ThemeDisplay themeDisplay = (ThemeDisplay) 
				portletRequest.getAttribute(
						WebKeys.THEME_DISPLAY);
		return getGroupId(themeDisplay, inParent);
	}
	
	public static long getGroupId(ThemeDisplay themeDisplay) {
		return getGroupId(themeDisplay, false);
	}
	
	public static long getGroupId(ThemeDisplay themeDisplay, 
			boolean inParent) {
		long groupId = 0;
		if (inParent) {
			if (themeDisplay.getScopeGroup(
					).getParentGroupId() > 0) {
				groupId = themeDisplay.getScopeGroup(
						).getParentGroupId();
			} else {
				groupId = themeDisplay.getScopeGroupId();
			}
		} else {
			groupId = themeDisplay.getScopeGroupId();
		}//
		return groupId;
	}
	
}
