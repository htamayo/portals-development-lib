package co.development.portals.liferay;

import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portlet.journal.model.JournalFolder;
import com.liferay.portlet.journal.service.JournalFolderLocalServiceUtil;

/**
 * 
 * @author H. Tamayo
 * @editor H. Tamayo
 * @last_edition 14/06/2017
 *
 */
public class JournalFolderUtil {

	/**
	 * @deprecated
	 * The parameter portletRequest is not longer used in this method.
	 * 
	 * @param portletRequest
	 * @param name
	 * @param groupId
	 * @return
	 * @throws SystemException
	 */
	@Deprecated
	public static JournalFolder getJournalFolderByName(
			PortletRequest portletRequest, String name, 
			Long groupId) throws SystemException {
		return getJournalFolderByName(name, groupId);
	}
	
	@SuppressWarnings("unchecked")
	public static JournalFolder getJournalFolderByName(
			String name, Long groupId) throws SystemException {
		DynamicQuery dynamicQuery = 
				DynamicQueryFactoryUtil.forClass(
						JournalFolder.class, 
						PortalClassLoaderUtil.getClassLoader());
		if (groupId != null) 
			dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", 
					groupId));
		if (name != null) 
			dynamicQuery.add(RestrictionsFactoryUtil.eq("name", 
					name));
		//
		List<JournalFolder> journalFolders = 
				JournalFolderLocalServiceUtil.dynamicQuery(
						dynamicQuery);
		//
		if (journalFolders != null ? journalFolders.size() > 0 : false) {
			return journalFolders.get(0);
		} else {
			return null;
		}
	}
	
}
