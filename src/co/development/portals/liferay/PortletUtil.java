package co.development.portals.liferay;

import java.io.IOException; 

import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.Validator;

/**
 * 
 * @author H. Tamayo
 * @editor H. Tamayo
 * @last_edition 15/02/2017
 *
 */
public class PortletUtil {

	public static void downloadPdfFile(
			ResourceResponse resourceResponse, String fileName, 
			byte[] byteArray) throws IOException {
		downloadPdfFile(resourceResponse, fileName, null, byteArray);
	}
	
	public static void downloadPdfFile(
			ResourceResponse resourceResponse, String fileName, 
			String cacheControl, byte[] byteArray) 
					throws IOException {
		downloadFile(resourceResponse, PDF_CONTENT_TYPE, 
				FILE_CONTENT_DISPOSITION + fileName, cacheControl, 
				byteArray);
	}
	
	public static void downloadFile(
			ResourceResponse resourceResponse, String contentType, 
			String contentDisposition, String cacheControl, 
			byte[] byteArray) throws IOException {
		resourceResponse.setContentType(contentType);
		//
		if (Validator.isNotNull(contentDisposition)) {
			resourceResponse.addProperty(
					HttpHeaders.CONTENT_DISPOSITION, 
					contentDisposition);
		}
		//
		if (Validator.isNotNull(cacheControl)) {
			resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL, 
					cacheControl);
		}
		//
		resourceResponse.getPortletOutputStream().write(byteArray);
	}
	
	public static final String PDF_CONTENT_TYPE = "application/pdf";
	public static final String FILE_CONTENT_DISPOSITION = 
			"attachment;filename=";
	
}
