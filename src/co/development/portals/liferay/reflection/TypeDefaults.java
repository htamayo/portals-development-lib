package co.development.portals.liferay.reflection;

/**
 * 
 * @author H. Tamayo
 * @editor H. Tamayo
 * @last_edition 5 de ago. de 2016
 *
 */
public class TypeDefaults {
    // These gets initialized to their default values
    private static Boolean DEFAULT_BOOLEAN = false;
    private static Byte DEFAULT_BYTE = 0;
    private static Short DEFAULT_SHORT = 0;
    private static Integer DEFAULT_INT = 0;
    private static Long DEFAULT_LONG = Long.valueOf(0);
    private static Float DEFAULT_FLOAT = Float.valueOf(0);
    private static Double DEFAULT_DOUBLE = Double.valueOf(0);
    private static String DEFAULT_STRING = "";

    public static <T> Object getDefaultValue(Class<T> clazz) {
        if (clazz.equals(boolean.class) || clazz.equals(
        		Boolean.class)) {
            return (T)DEFAULT_BOOLEAN;
        } else if (clazz.equals(byte.class) || clazz.equals(
        		Byte.class)) {
            return (T)DEFAULT_BYTE;
        } else if (clazz.equals(short.class) || clazz.equals(
        		Short.class)) {
            return (T)DEFAULT_SHORT;
        } else if (clazz.equals(int.class) || clazz.equals(
        		Integer.class)) {
            return (T)DEFAULT_INT;
        } else if (clazz.equals(long.class) || clazz.equals(
        		Long.class)) {
            return (T)DEFAULT_LONG;
        } else if (clazz.equals(float.class) || clazz.equals(
        		Float.class)) {
            return (T)DEFAULT_FLOAT;
        } else if (clazz.equals(double.class) || clazz.equals(
        		Double.class)) {
            return (T)DEFAULT_DOUBLE;
        } else if (clazz.equals(String.class)){
        	return (T)DEFAULT_STRING;
        } else {
            return null;
        }
    }
}