package co.development.portals.liferay.reflection;

import com.liferay.portal.kernel.util.ClassResolverUtil;
import com.liferay.portal.kernel.util.MethodKey;
import com.liferay.portal.kernel.util.PortletClassInvoker;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

/** 
 * 
 * @author H. Tamayo 
 * @last_edition 2/10/2018
 *
 */
public class UseReflectionAPI {
	// 
	public static Class<?>[] convertToArray(Class<?>... classes) {
		return classes;
	}
	//
	public static long getGroupId(PortletRequest portletRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay)
				portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		//
		return ((Long)getPortletRequestAttribute(portletRequest, 
				GROUP_ID, themeDisplay.getScopeGroupId())).longValue();
	}
	// 
	public static String getRedirectPortlet(
			PortletRequest portletRequest, String defaultPortlet) {
		return (String)getPortletRequestAttribute(portletRequest, 
				REDIRECT_PORTLET_ID, defaultPortlet);
	}
	// 
	public static String getRenderJspPage(
			PortletRequest portletRequest, String defaultValue) {
		return (String)getPortletRequestAttribute(portletRequest, 
				RENDER_JSP_PAGE, defaultValue);
	}
	// 
	public static Boolean isReflectionCall(
			PortletRequest portletRequest) {
		return (Boolean)getPortletRequestAttribute(portletRequest, 
				IS_REFLECTION_CALL, false);
	}
	  
	public static Boolean isUseDestinationSuperMethod(
			PortletRequest portletRequest) {
		return (Boolean)getPortletRequestAttribute(portletRequest, 
				USE_DESTINATION_SUPER_METHOD, true);
	}
	// 
	@SuppressWarnings("unchecked")
	public static <T> T getPortletRequestAttribute(
			PortletRequest portletRequest, String attributeKey, 
			Object defaultValue) {
		Object obj = portletRequest.getAttribute(attributeKey);
	    Object object = null;
	    if (obj != null) {
	      object = obj;
	    } else {
	      object = defaultValue;
	    }
	    return (T)object;
	}
	// 	
	public static void setGroupId(PortletRequest portletRequest, 
			boolean isParent) { 
		ThemeDisplay themeDisplay = (ThemeDisplay)
				portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		// 
		if (isParent) {
			portletRequest.setAttribute(GROUP_ID, 
					themeDisplay.getScopeGroup().getParentGroupId());
		} else {
			portletRequest.setAttribute(GROUP_ID, 
					themeDisplay.getScopeGroupId());
	    }
	}
	// 
	public static void setGroupId(PortletRequest portletRequest) {
	    setGroupId(portletRequest, false); 
    }
	// 
	public static void setParentGroupId(
			PortletRequest portletRequest) { 
	    setGroupId(portletRequest, true);
	}
	// 
	public static void setReflectionCall(
			PortletRequest portletRequest) { 
	    setReflectionCall(portletRequest, true);
	}
	// 
	public static void setReflectionCall(
			PortletRequest portletRequest, 
			Boolean isReflectionCall) { 
		portletRequest.setAttribute(IS_REFLECTION_CALL, 
				isReflectionCall);
	}
	// 
	public static void setRenderJspPage(
			PortletRequest portletRequest, String renderJspPage) { 
	    portletRequest.setAttribute(RENDER_JSP_PAGE, 
	    		renderJspPage);
	}
	// 
	public static void setResultMessage(
			PortletRequest portletRequest, String message) { 
	    if (isReflectionCall(portletRequest).booleanValue()) {
	    	portletRequest.setAttribute(
	    			DESTINATION_RESULT_MESSAGE, 
	    			message != null ? message : "");
	    }
	}
	// 
	public static void setSuccessMessage(
			PortletRequest portletRequest) {
		setResultMessage(portletRequest, SUCCESS);
	}
	// 
	public static void setUseDestinationSuperMethod(
			PortletRequest portletRequest) { 
	    setUseDestinationSuperMethod(portletRequest, false);
	}
	// 
	public static void setUseDestinationSuperMethod(
			PortletRequest portletRequest, 
			Boolean useDestinationSuperMethod) { 
		portletRequest.setAttribute(USE_DESTINATION_SUPER_METHOD, 
				useDestinationSuperMethod);
	}
	// 
	public static String useReflection(String portletContext, 
			String portletId, String className, String methodName, 
			Class<?>[] parameters, Object... values) throws Exception { 
		UseReflectionAPI useReflectionAPI = new UseReflectionAPI(
				portletContext, portletId, className, methodName, 
				parameters, values);
	    // 
	    return useReflection(useReflectionAPI);
	}
	// 
	public static String useReflectionToDoView(
			UseReflectionAPI useReflectionAPI, String className, 
			RenderRequest renderRequest, 
			RenderResponse renderResponse) throws Exception { 
		setUseDestinationSuperMethod(renderRequest); 
		//
	    useReflectionAPI.setClassName(className);
	    useReflectionAPI.setMethodName(DO_VIEW);
	    useReflectionAPI.setParameters(DO_VIEW_PARAMETERS);
	    useReflectionAPI.setValues(new Object[] { 
	    		renderRequest, renderResponse 
		});
	    // 
	    return useReflection(useReflectionAPI);
	}
	// 
	public static String useReflectionToProcessAction(
			UseReflectionAPI useReflectionAPI, String className, 
			String methodName, ActionRequest actionRequest, 
			ActionResponse actionResponse) throws Exception { 
	    useReflectionAPI.setClassName(className);
	    useReflectionAPI.setMethodName(methodName);
	    useReflectionAPI.setParameters(PROCESS_ACTION_PARAMETERS);
	    useReflectionAPI.setValues(new Object[] { 
	    		actionRequest, actionResponse 
	    });
	    // 
	    return useReflection(useReflectionAPI);
	}
	// 
	public static String useReflectionToServeResource(
			UseReflectionAPI useReflectionAPI, String className, 
			ResourceRequest resourceRequest, 
			ResourceResponse resourceResponse) throws Exception { 
		setUseDestinationSuperMethod(resourceRequest);
		//
	    useReflectionAPI.setClassName(className);
	    useReflectionAPI.setMethodName(SERVE_RESOURCE);
	    useReflectionAPI.setParameters(SERVE_RESOURCE_PARAMETERS);
	    useReflectionAPI.setValues(new Object[] { 
	    		resourceRequest, resourceResponse 
	    });
	    // 
	    return useReflection(useReflectionAPI);
	}
	// 
	public static <T> T useReflectionToSome(Class<T> clazz, 
			UseReflectionAPI useReflectionAPI, String className, 
			String methodName, Class<?>[] parameters, Object... values)
					throws Exception { 
	    useReflectionAPI.setClassName(className);
	    useReflectionAPI.setMethodName(methodName);
	    useReflectionAPI.setParameters(parameters);
	    useReflectionAPI.setValues(values);
	    // 
	    return useReflection(useReflectionAPI, clazz);
	}
	// 
	public static String useReflectionToSome(
			UseReflectionAPI useReflectionAPI, String className, 
			String methodName, Class<?>[] parameters, Object... values)
					throws Exception { 
	    useReflectionAPI.setClassName(className);
	    useReflectionAPI.setMethodName(methodName);
	    useReflectionAPI.setParameters(parameters);
	    useReflectionAPI.setValues(values);
	    // 
	    return useReflection(useReflectionAPI);
	}
	// 
	public static String useReflection(
			UseReflectionAPI useReflectionAPI) throws Exception { 
	    return (String)useReflection(useReflectionAPI, String.class);
	}
	// 
	@SuppressWarnings("unchecked")
	public static <T> T useReflection(
			UseReflectionAPI useReflectionAPI, Class<T> clazz) 
					throws Exception { 
	    Object returnObj = 
	      useReflectionAPI.invokeByPortletClassLoader();
	    
	    if (returnObj.equals(Void.TYPE)) {
	      if (clazz.equals(String.class)) {
	        return (T)SUCCESS_PORTLET_ATTRIBUTE;
	      }
	      return null;
	    }
	    // 
	    return (T)returnObj;
	}
	// 
	public Object invokeByPortletClassLoader() throws Exception { 
	    if (className.isEmpty()) {
	      throw new Exception("The Class Name not can be empty");
	    }
		if (portletContext.isEmpty()) {
		  throw new Exception(
		    "The Portlet Context not can be empty");
		}
		if (methodName.isEmpty()) {
		  throw new Exception(
		    "The Method Name not can be empty");
		}
		Class<?> clazz = 
				ClassResolverUtil.resolveByPortletClassLoader(
						className, portletContext);
		//
		MethodKey method = null;
		
		if (parameters != null) {
			if (parameters.length > 0) {
				if (parameters.length == values.length) {
					method = new MethodKey(clazz, methodName, 
							parameters);
				} else {
					throw new Exception("The parameters object and "
							+ "values object not are equals");
				}
			} else if ((values == null) || (values.length <= 0)) {
				method = new MethodKey(clazz, methodName, new Class[0]);
			} else {
				throw new Exception("The parameters object and "
						+ "values object not are equals");
			}
		} else if (values == null) {
			method = new MethodKey(clazz, methodName, new Class[0]);
		} else {
			throw new Exception("The parameters object and values "
					+ "object not are equals");
	    }
		// 
	    Object objReturn = null;
	    if (method.getMethod().getReturnType().equals(Void.TYPE)) {
	    	PortletClassInvoker.invoke(true, portletId, method, 
	    			values);
	    	//
	    	objReturn = Void.TYPE;
	    } else {
	    	objReturn = PortletClassInvoker.invoke(true, portletId, 
	    			method, values);
	    }
	    // 
	    return objReturn;
	}
	// 
	public static void validateStringMethod(String resultStr) 
			throws Exception {
		if (resultStr.contains(ERROR)) {
			throw new Exception(resultStr);
		}
	}
	// 
	public static void validateVoidMethod(
			PortletRequest portletRequest, String resultStr) 
					throws Exception { 
	    String resultMessage = "";
	    if (resultStr.equals(SUCCESS_PORTLET_ATTRIBUTE)) {
	    	resultMessage = (String)portletRequest.getAttribute(
	    			DESTINATION_RESULT_MESSAGE);
	    	//
    		if (resultMessage != null) {
    			if (!resultMessage.equals(SUCCESS)) {
    				throw new Exception(resultMessage);
    			}
    		} else {
    			throw new NullPointerException("Result Message is "
    					+ "null, something wrong happend in the "
    					+ "method called");
    		}
    	} else {
	      throw new Exception(resultStr);
	    }
	}
	// 
	public UseReflectionAPI() {
		super(); 
	}
	// 
	public UseReflectionAPI(String portletContext, String portletId, 
			String className, String methodName, Class<?>[] parameters, 
			Object... values) {
		UseReflectionAPI useReflection = new UseReflectionAPI();
	    // 
	    this.portletContext = portletContext;
	    this.portletId = portletId;
	    this.className = className;
	    this.methodName = methodName;
	    this.parameters = parameters;
	    this.values = values;
	}
	// 
	public String getClassName() {
		return className;
	}
	// 
	public void setClassName(String className) {
	    this.className = className;
	}
	// 
	public Class<?>[] getParameters() {
	    return parameters;
	}
	// 
	public void setParameters(Class<?>... parameters) {
	    this.parameters = parameters;
	}
	// 
	public String getPortletContext() {
	    return portletContext;
	}
	// 
	public void setPortletContext(String portletContext) {
	    this.portletContext = portletContext;
	}
	// 
	public String getPortletId() {
	    return portletId;
	}
	//
	public void setPortletId(String portletId) {
	    this.portletId = portletId;
	}
	// 
	public String getMethodName() {
	    return methodName;
	}
	
	public void setMethodName(String methodName) {
	    this.methodName = methodName;
	}
	// 
	public Object[] getValues() {
	    return values;
	}
	// 
	public void setValues(Object... values) {
	    this.values = values;
	}
	// 
	private String className;
	private Class<?>[] parameters;
	private String portletContext;
	private String portletId;
	private String methodName;
	private Object[] values;
	public static final String DO_VIEW = "doView";
	//
	public static final Class<?>[] DO_VIEW_PARAMETERS = {
			RenderRequest.class, RenderResponse.class 
	};	
	public static final String PROCESS_ACTION = "processAction";
	public static final Class<?>[] PROCESS_ACTION_PARAMETERS = {
			ActionRequest.class, ActionResponse.class 
	};	  
	public static final String SERVE_RESOURCE = "serveResource";
	public static final Class<?>[] SERVE_RESOURCE_PARAMETERS = {
			ResourceRequest.class, ResourceResponse.class 
	};
	public static final String DESTINATION_RESULT_MESSAGE = 
			"destinationResultMessage";
	public static final String ERROR = "[ERROR]: ";
	public static final String FAIL_RENDER_JSP_PAGE = 
			"failRenderJspPage";
	public static final String GROUP_ID = "groupId";
	public static final String IS_REFLECTION_CALL = 
			"isReflectionCall";
	public static final String REDIRECT_ACTION_NAME = 
			"redirectActionName";
	public static final String REDIRECT_PORTLET_ID = 
			"redirectPortletId";
	public static final String RENDER_JSP_PAGE = "renderJspPage";
	public static final String SECOND_RENDER_JSP_PAGE = 
			"secondRenderJspPage";
	public static final String SUCCESS = "SUCCESS";
	public static final String SUCCESS_RENDER_JSP_PAGE = 
			"successRenderJspPage";
	public static final String SUCCESS_PORTLET_ATTRIBUTE = 
			"SUCCESS_PORTLET_ATTRIBUTE";
	public static final String THIRD_RENDER_JSP_PAGE = 
			"thirdRenderJspPage";
	public static final String USE_DESTINATION_SUPER_METHOD = 
			"useDestionationSuperMethod";
	
}
