package co.development.portals.liferay;


import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;

/**
 * 
 * @author H. Tamayo
 * @editor H. Tamayo
 * @last_edition 28 de oct. de 2016
 *
 */
public class LiferayPortletURLUtil {

	public static void sendRedirectLiferayPortletURL(
			ActionResponse actionResponse, 
			LiferayPortletURL liferayPortletUrl)
		throws IOException {
		actionResponse.sendRedirect(liferayPortletUrl.toString());
	}
	
	public static LiferayPortletURL getURLToExecutePortletAction(
			PortletResponse portletResponse, long groupId, 
			String portletId, String actionName)
		throws PortalException, SystemException {
		return getURLToExecutePortletAction(portletResponse, 
				groupId, portletId, actionName, null);
	}
	
	/**
	 * Only applicable to a portlet non-instanceable
	 * 
	 * @param portletResponse
	 * @param portletId
	 * @param groupId
	 * @return
	 */
	public static LiferayPortletURL getURLToExecutePortletAction(
			PortletResponse portletResponse, long groupId, 
			String portletId, String actionName, 
			Map<String, String> params) 
		throws PortalException, SystemException {
		LiferayPortletResponse liferayPortletResponse = 
				PortalUtil.getLiferayPortletResponse(
						portletResponse);
		LiferayPortletURL liferayPortletUrl = 
				liferayPortletResponse.createActionURL(portletId);
		//
		liferayPortletUrl.setPlid(
				PortalUtil.getPlidFromPortletId(groupId, portletId));
		//
		liferayPortletUrl.setParameter(ActionRequest.ACTION_NAME, 
				actionName);
		//
		loadParamsInLiferayPortletURL(params, liferayPortletUrl);
		//
		return liferayPortletUrl;
	}
	
	public static LiferayPortletURL getURLToExecutePortletServeResource(
			PortletResponse portletResponse, long groupId, 
			String portletId, String resourceId)
		throws PortalException, SystemException {
		return getURLToExecutePortletServeResource(portletResponse, 
				groupId, portletId, resourceId, null);
	}
	
	public static LiferayPortletURL getURLToExecutePortletServeResource(
			PortletResponse portletResponse, long groupId, 
			String portletId, String resourceId, 
			Map<String, String> params)
		throws PortalException, SystemException {
		LiferayPortletResponse liferayPortletResponse = 
				PortalUtil.getLiferayPortletResponse(portletResponse);
		LiferayPortletURL liferayPortletUrl = 
				liferayPortletResponse.createResourceURL(portletId);
		//
		liferayPortletUrl.setPlid(
				PortalUtil.getPlidFromPortletId(groupId, 
						portletId));
		//
		liferayPortletUrl.setResourceID(resourceId);
		//
		loadParamsInLiferayPortletURL(params, liferayPortletUrl);
		//
		return liferayPortletUrl;
	}
	
	public static LiferayPortletURL getURLWherePortletIsLocated(
			PortletRequest portletRequest, long groupId, 
			String portletId) 
		throws PortalException, SystemException {
		return getURLWherePortletIsLocated(portletRequest, groupId, 
				portletId, null);
	}
	
	public static LiferayPortletURL getURLWherePortletIsLocated(
			PortletRequest portletRequest, long groupId, 
			String portletId, Map<String, String> params)
		throws PortalException, SystemException {
		return getURLWherePortletIsLocated(portletRequest, groupId, 
				portletId, params, PortletRequest.RENDER_PHASE);
	}
	
	public static LiferayPortletURL getURLWherePortletIsLocated(
			PortletRequest portletRequest, long groupId, 
			String portletId, Map<String, String> params, 
			String portletPhase)
		throws PortalException, SystemException {
		long plid = PortalUtil.getPlidFromPortletId(groupId, 
				portletId);
		LiferayPortletURL liferayPortletUrl = 
				PortletURLFactoryUtil.create(portletRequest, 
						portletId, plid, portletPhase);
		//
		loadParamsInLiferayPortletURL(params, liferayPortletUrl);
		//
		return liferayPortletUrl;
	}
	
	public static void loadParamsInLiferayPortletURL(
			Map<String, String> params, 
			LiferayPortletURL liferayPortletUrl) {
		if (params != null) {
			Iterator<Entry<String, String>> iterator = 
					params.entrySet().iterator();
			while(iterator.hasNext()) {
				Map.Entry<String, String> param = 
						(Entry<String, String>)iterator.next();
				if (param.getKey() != null && 
						param.getValue() != null) {
					liferayPortletUrl.setParameter(param.getKey(),
							param.getValue());
				} else {
					_log.error("KEY OR VALUR IS NULL: Key: " + 
							String.valueOf(param.getKey()) + " | " + 
							"Value: " + String.valueOf(
									param.getValue()));
				}
				//
				
				//
				iterator.remove();
			}
		}
	}

	public static Map<String, String> getParametersFromCurrentURL(
			PortletRequest portletRequest, 
			Map<String, String> params) {
		String url = PortalUtil.getCurrentURL(portletRequest);
		//
		return getParametersFromURL(portletRequest, params, url);
	}
	
	public static Map<String, String> getParametersFromURL(
			PortletRequest portletRequest, 
			Map<String, String> params, String url) {
		if (params == null)
			params = new HashMap<String, String>();
		//
		loadUrlParameters(params, url);
		//
		return params;
	}
	
	public static void loadUrlParameters(
			Map<String, String> params, String url) {
		if (url.matches(".*[?].*")) {
			String[] paramsArray = 
					url.split("\\x3F",-1)[1].split("&");
			//
			for (String param : paramsArray) {
				String[] tempParamArray = param.split("=");
				//
				if (Validator.isNotNull(tempParamArray[0]) &&
						Validator.isNotNull(tempParamArray[1])) {
					params.put(
							tempParamArray[0],
							tempParamArray[1]);
				}
			}
		}
	}

	private static Log _log = LogFactoryUtil.getLog(
			LiferayPortletURLUtil.class);
}
