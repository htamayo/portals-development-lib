package co.development.portals.liferay.portlet;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.threeten.bp.LocalDate;
import org.threeten.bp.YearMonth;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import co.development.portals.liferay.servlet.SessionMessagesUtil;

/**
 * 
 * @author H. Tamayo 
 * @last_edition 30/06/2019
 *
 */
public class MVCCommonPortlet extends MVCPortlet {
	// 
	// #region Properties 
	private static Log _log = LogFactoryUtil.getLog(
			MVCCommonPortlet.class); 
	//
	private boolean requestDates = false; 
	//
	protected final String MVC_PATH = "mvcPath";
	protected final String JSP_PAGE = "jspPage"; 
	// 
	public static final String INITIAL_DATE_ATTR = "initialDateObject"; 
	public static final String FINAL_DATE_ATTR = "finalDateObject"; 
	// #endregion
	// 
	// #region Getters and Setters 
	public boolean isRequestDates() {
		return requestDates; 
	}
	public void setRequestDates(boolean requestDates) {
		this.requestDates = requestDates; 
	}
	// #endregion 
	//
	// #region Util Methods
	//
	public static void loadUrlParameters(
			Map<String, String> params, String url) {
		if (url.matches(".*[?].*")) {
			String[] paramsArray = 
					url.split("\\x3F",-1)[1].split("&");
			//
			for (String param : paramsArray) {
				String[] tempParamArray = param.split("=");
				//
				if (Validator.isNotNull(tempParamArray[0]) &&
						Validator.isNotNull(tempParamArray[1])) {
					params.put(
							tempParamArray[0],
							tempParamArray[1]);
				}
			}
		}
	}
	//
	// #endregion
	//
	@Override
	public void processAction(ActionRequest actionRequest, 
			ActionResponse actionResponse) 
		throws IOException, PortletException {
		SessionMessages.add(actionRequest, 
				PortalUtil.getPortletId(actionRequest) + 
				SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		SessionMessages.add(actionRequest, 
				PortalUtil.getPortletId(actionRequest) +
				SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_SUCCESS_MESSAGE);
		//
		super.processAction(actionRequest, actionResponse);
	}
	
	@Override
	public void doView(RenderRequest renderRequest, 
			RenderResponse renderResponse) 
		throws IOException, PortletException {
		SessionMessages.add(renderRequest, 
				PortalUtil.getPortletId(renderRequest) + 
				SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE); 
		// 
		if (this.requestDates) {
			try {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				//
				YearMonth yearMonth = YearMonth.of(calendar.get(
						Calendar.YEAR), calendar.get(Calendar.MONTH) + 1);
				LocalDate initialDate = yearMonth.atDay(1);
				LocalDate finalDate = yearMonth.atEndOfMonth();
				//
				renderRequest.setAttribute(INITIAL_DATE_ATTR, initialDate);
				renderRequest.setAttribute(FINAL_DATE_ATTR, finalDate);
			} catch (Exception ex) {
				String msg = "Error setting start and end dates: "
						+ ex.getMessage();
				//
				if (_log.isDebugEnabled()) 
					_log.error(msg, ex);
				else 
					_log.error(msg);
				// 
				SessionMessagesUtil.setElementError(renderRequest, 
						"Fechas inicial y final");
			}
		}
		//
		super.doView(renderRequest, renderResponse);
	} 
}
