package co.development.portals.liferay.datasource;

import javax.sql.DataSource;

/**
 * 
 * @author H. Tamayo 
 * @last_edition 10/07/2019
 *
 */
public class NoDataSource {
	
	/**
	 * 
	 * @return Always null.	
	 */
	public static DataSource getDataSource() {
		return null;
	}
	
}
