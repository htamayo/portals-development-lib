package co.development.portals.liferay;

import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Order;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalArticleDisplay;
import com.liferay.portlet.journal.model.JournalFolder;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.journalcontent.util.JournalContentUtil;

/**
 * 
 * @author H. Tamayo
 * @last_edition 7/04/2017
 *
 */
public class JournalArticleUtil {
	
	public static String 
		getContentFromJournalArticleDisplayByTitleInEsOrderedByStatusDateDesc(
			Long groupId, String name, 
			ThemeDisplay themeDisplay) 
		throws SystemException {
		return getJournalArticleDisplayByTitleInEsOrderedByStatusDateDesc(
				groupId, name, themeDisplay).getContent();
	}
	
	public static String getContentFromJournalArticleDisplay(
			Long groupId, String articleId, 
			ThemeDisplay themeDisplay) {
		return getJournalArticleDisplay(groupId, articleId, 
				themeDisplay).getContent();
	}
	
	public static JournalArticleDisplay 
		getJournalArticleDisplayByTitleInEsOrderedByStatusDateDesc(
				Long groupId, String name, 
				ThemeDisplay themeDisplay)
		throws SystemException {
		return getJournalArticleDisplay(groupId, 
				getJournalArticleByTitleInEsOrderedByStatusDateDesc(
						groupId, name).getArticleId(), 
				themeDisplay);
	}
	
	public static JournalArticleDisplay 
		getJournalArticleDisplay(Long groupId, String articleId, 
				ThemeDisplay themeDisplay) {
		return getJournalArticleDisplay(groupId, articleId, 
				StringPool.BLANK, StringPool.BLANK, 
				themeDisplay);
	}
	
	public static JournalArticleDisplay 
		getJournalArticleDisplay(Long groupId, String articleId, 
				String viewMode, String languageId, 
				ThemeDisplay themeDisplay) {
		JournalArticleDisplay journalArticleDisplay = 
				JournalContentUtil.getDisplay(
						groupId, articleId, 
						viewMode, languageId, 
						themeDisplay);
		//
		return journalArticleDisplay;
	}
	
	/**
	 * Gets THEME_DISPLAY Object from Request.
	 * 
	 * @param portletRequest
	 * @param name
	 * @param attributeKey
	 * @throws SystemException
	 */
	public static void 
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				PortletRequest portletRequest, String name, 
				String attributeKey)
		throws SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) 
				portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		//
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				portletRequest, themeDisplay, name, attributeKey);
	}
	
	/**
	 * Use the last override method with 
	 * themeDisplay.getScopeGroupId()
	 * 
	 * @param portletRequest
	 * @param themeDisplay
	 * @param journalArticleName
	 * @param journalArticleAttributeKey
	 */
	public static void 
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				PortletRequest portletRequest, 
				ThemeDisplay themeDisplay, String name, 
				String attributeKey)
		throws SystemException {
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				portletRequest, themeDisplay.getScopeGroupId(),	
				name, attributeKey);
	}
	
	/**
	 * This method uses setRequestJournalArticleByTitleInEs
	 * OrderedByStatusDateDesc overloaded, setting 
	 * validateInSession as true.
	 * 
	 * @param portletRequest
	 * @param groupId
	 * @param name
	 * @param attributeKey
	 * @throws SystemException
	 */
	public static void 
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
			PortletRequest portletRequest, Long groupId, 
			String name, String attributeKey) 
		throws SystemException {
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				portletRequest, groupId, name, attributeKey, 
				true);
	}
	
	/**
	 * This method uses setRequestJournalArticleByTitleInEs
	 * OrderedByStatusDateDesc overloaded, setting 
	 * validateInSession as true. It's very important keep in mind 
	 * that the object is validated in session but this not is 
	 * replaced.
	 * 
	 * @param portletRequest
	 * @param groupId
	 * @param name
	 * @param attributeKey
	 * @param validateInSession
	 * @throws SystemException
	 */
	public static void 
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				PortletRequest portletRequest, Long groupId, 
				String name, String attributeKey, 
				boolean validateInSession)
		throws SystemException {
		boolean saveInSession = validateInSession;
		//
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				portletRequest, groupId, name, attributeKey, 
				validateInSession, saveInSession, false);
	}
	
	/**
	 * This method uses setRequestJournalArticleId overloaded, 
	 * setting portletSessionScope as PORTLET_SCOPE.
	 * 
	 * @param portletRequest
	 * @param groupId
	 * @param name
	 * @param attributeKey
	 * @param validateInSession
	 * @param saveInSession
	 * @param replaceInSession
	 * @throws SystemException
	 */
	public static void 
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				PortletRequest portletRequest, Long groupId,
				String name, String attributeKey, 
				boolean validateInSession, boolean saveInSession, 
				boolean replaceInSession)
		throws SystemException {
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
				portletRequest, groupId, name, attributeKey, 
				validateInSession, saveInSession, replaceInSession, 
				PortletSession.PORTLET_SCOPE, false, null);
	}
	
	/**
	 * Original Method
	 * 
	 * @param portletRequest
	 * @param groupId
	 * @param name
	 * @param attributeKey
	 * @param validateInSession
	 * @param saveInSession
	 * @param replaceInSession
	 * @param portletSessionScope
	 * @throws SystemException
	 */
	public static void 
		setRequestJournalArticleByTitleInEsOrderedByStatusDateDesc(
			PortletRequest portletRequest, Long groupId, 
			String name, String attributeKey, 
			boolean validateInSession, boolean saveInSession, 
			boolean replaceInSession, int portletSessionScope, 
			boolean hasOptional, String optionalName)
		throws SystemException {
		JournalArticle journalArticle = null;
		//
		if (validateInSession) {
			PortletSession portletSession = 
					portletRequest.getPortletSession();
			Object sessionObject = portletSession.getAttribute(
					attributeKey, portletSessionScope);
			if (sessionObject != null) {
				if (replaceInSession) 
					portletSession.removeAttribute(
							attributeKey, portletSessionScope);
				else 
					journalArticle = (JournalArticle)sessionObject;
			}
		}
		//
		if (journalArticle == null) {
			try {
				journalArticle = 
					getJournalArticleByTitleInEsOrderedByStatusDateDesc(
							groupId, name);
			} catch (SystemException ex ) {
				if (hasOptional) {
					journalArticle = 
							getJournalArticleByTitleInEsOrderedByStatusDateDesc(
									groupId, optionalName);
				}
			}
		}
		//
		if (journalArticle != null) {
			if (!journalArticle.getArticleId().isEmpty()) {
				portletRequest.setAttribute(attributeKey, 
						journalArticle);
				//
				if (saveInSession) {
					PortletSession portletSession = 
							portletRequest.getPortletSession();
					//
					portletSession.setAttribute(attributeKey, 
							journalArticle, portletSessionScope);
				}
			} else {
				throw new SystemException(
						"Journal Article Id is Empty.");
			}
		} else {
			throw new SystemException(
					"Journal Article not has been found.");
		}
	}
	
	/**
	 * Validate the size of list and return the first result or 
	 * throws an SystemException (if the size isn't major to 
	 * 0
	 * 
	 * @param groupId
	 * @param name
	 * @return
	 * @throws SystemException
	 */
	public static JournalArticle 
		getJournalArticleByTitleInEsOrderedByStatusDateDesc(
				Long groupId, String name)
		throws SystemException {
		List<JournalArticle> journalArticles = 
				getJournalArticlesByTitleInEsOrderedByStatusDateDesc(
						groupId, name);
		//
		if (journalArticles.size() > 0)
			return journalArticles.get(0);
		else 
			throw new SystemException(
					"JournalArticle not found");
	}
	
	/**
	 * Send Order by statusDate and Order DESC
	 * 
	 * @param groupId
	 * @param name
	 * @return
	 * @throws SystemException
	 */
	public static List<JournalArticle> 
		getJournalArticlesByTitleInEsOrderedByStatusDateDesc(
				Long groupId, String name)
		throws SystemException {
		return getJournalArticlesByTitleInEs(groupId, name, 
				"statusDate", false);
	}
	
	/**
	 * Uses the JournalArticleUtil's constants to  
	 * Override Parameters
	 * 
	 * @param groupId
	 * @param name
	 * @param orderBy
	 * @param isAsc
	 * @return
	 * @throws SystemException
	 */
	public static List<JournalArticle> 
		getJournalArticlesByTitleInEs(
			Long groupId, String name, String orderBy, 
			Boolean isAsc)
					throws SystemException {
		return getJournalArticlesByTitle(FIRST_XML_VERSION, 
				UTF8_ENCODING, ES_AVAILABLE_LOCALE, ES_LOCALE, 
				ES_LANGUAGE_ID, groupId, name, orderBy, isAsc);
	}
	
	/**
	 * Send the folderId parameter as Null
	 * 
	 * @param xmlVersion
	 * @param encoding
	 * @param availableLocale
	 * @param locale
	 * @param languageId
	 * @param groupId
	 * @param name
	 * @param orderBy
	 * @param isAsc
	 * @return
	 * @throws SystemException
	 * @{@link JournalArticleUtil.getJournalArticleByFolderAndTitle}
	 */
	public static List<JournalArticle> getJournalArticlesByTitle(
			String xmlVersion, String encoding, 
			String availableLocale, String locale, 
			String languageId, Long groupId, String name, 
			String orderBy, Boolean isAsc)
					throws SystemException {
		return getJournalArticlesByFolderAndTitle(
				xmlVersion, encoding, availableLocale, locale, 
				languageId, groupId,
				// null folder
				null, name, orderBy, isAsc);
	}
	
	/**
	 * 
	 * @param groupId
	 * @param folderName To search the JournalFolder for name
	 * @param name
	 * @return
	 * @throws SystemException
	 */
	public static JournalArticle 
	getJournalArticleByFolderAndTitle(Long groupId, 
			String folderName, String name) 
						throws SystemException {
		JournalFolder journalFolder = 
				JournalFolderUtil.getJournalFolderByName(
						folderName, groupId);
		//
		if (journalFolder != null) {
			return getJournalArticleByFolderAndTitle(groupId, 
					journalFolder.getFolderId(), name);
		} else {
			throw new SystemException("JournalFolder " + 
					folderName + " not found.");
		}
	}
	
	/**
	 *  Validate the size of list and return the first result or 
	 * throws an SystemException (if the size isn't major to 
	 * 0
	 * 
	 * @param groupId
	 * @param folderId
	 * @param name
	 * @return
	 * @throws SystemException
	 */
	public static JournalArticle 
	getJournalArticleByFolderAndTitle(Long groupId, 
			Long folderId, String name) 
					throws SystemException {
		List<JournalArticle> journalArticleList = 
				getJournalArticlesByFolderAndTitle(groupId, 
						folderId, name);
		//
		if (journalArticleList.size() > 0) {
			return journalArticleList.get(0);
		} else {
			throw new SystemException(
					"JournalArticle Not Found");
		}
	}
	
	/**
	 * Send Order by statusDate and Order DESC
	 * 
	 * @param groupId
	 * @param folderId
	 * @param name
	 * @return
	 * @throws SystemException
	 */
	public static List<JournalArticle> 
		getJournalArticlesByFolderAndTitle(
		Long groupId, Long folderId, String name) 
				throws SystemException {
	return getJournalArticlesByFolderAndTitle(
			FIRST_XML_VERSION, UTF8_ENCODING, 
			ES_AVAILABLE_LOCALE, ES_LOCALE, 
			ES_LANGUAGE_ID, groupId, folderId, name, 
			"statusDate", false); 
	}
	
	/**
	 * Uses the JournalArticleUtil's constants to  
	 * Override Parameters
	 * 
	 * @param groupId
	 * @param folderId
	 * @param name
	 * @param orderBy
	 * @param isAsc
	 * @return
	 * @throws SystemException
	 */
	public static List<JournalArticle> 
			getJournalArticlesByFolderAndTitle(
			Long groupId, Long folderId, String name, 
			String orderBy, Boolean isAsc) 
					throws SystemException {
		return getJournalArticlesByFolderAndTitle(
				FIRST_XML_VERSION, UTF8_ENCODING, 
				ES_AVAILABLE_LOCALE, ES_LOCALE, 
				ES_LANGUAGE_ID, groupId, folderId, name, 
				orderBy, isAsc); 
	}
	
	@SuppressWarnings("unchecked")
	public static List<JournalArticle> 
			getJournalArticlesByFolderAndTitle(
			String xmlVersion, String encoding, 
			String availableLocale, String locale, 
			String languageId, Long groupId, Long folderId, 
			String name, String orderBy, Boolean isAsc) 
					throws SystemException {
		String title = null;
		if (name != null) {
			title = getJournalArticleTitle(xmlVersion, encoding, 
					availableLocale, locale, languageId, name);
		}
		//
		DynamicQuery dynamicQuery = 
				DynamicQueryFactoryUtil.forClass(
						JournalArticle.class, 
						PortalClassLoaderUtil.getClassLoader());
		//
		if (Validator.isNotNull(groupId)) {
			dynamicQuery.add(RestrictionsFactoryUtil.eq(
					"groupId", groupId));
		}
		if(Validator.isNotNull(folderId)) {
			dynamicQuery.add(RestrictionsFactoryUtil.eq(
					"folderId", folderId));
		}
		if (Validator.isNotNull(title)) {
			dynamicQuery.add(RestrictionsFactoryUtil.eq(
					"title", title));
		}
		if (Validator.isNotNull(orderBy)) {
			Order order = null;
			if (isAsc) 
				order = OrderFactoryUtil.asc(orderBy);
			else 
				order = OrderFactoryUtil.desc(orderBy);
			//
			dynamicQuery.addOrder(order);
		}
		//
		List<JournalArticle> journalArticleList = 
				(List<JournalArticle>)
				JournalArticleLocalServiceUtil.dynamicQuery(
						dynamicQuery);
		//
		return journalArticleList; 
	}
	
	public static String getJournalArticleTitle(
			String xmlVersion, String encoding, 
			String availableLocale, String locale, 
			String languageId, String name) {
		return "<?xml version='" + xmlVersion + "' " + 
				"encoding='" + encoding + "'?>" + 
				"<root available-locales=\"" + 
				availableLocale + "\" " + 
				"default-locale=\"" + locale + 
				"\"><Title language-id=\"" + 
				languageId + "\">" + 
				name + 
				"</Title></root>";
	}
	
	public static final String FIRST_XML_VERSION = "1.0";
	public static final String UTF8_ENCODING = "UTF-8";
	public static final String ES_AVAILABLE_LOCALE = "es_ES";
	public static final String ES_LOCALE = "es_ES";
	public static final String ES_LANGUAGE_ID = "es_ES";
	
}
