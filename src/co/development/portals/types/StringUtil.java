package co.development.portals.types;

import com.liferay.portal.kernel.util.StringPool;

/**
 * 
 * @author 	H. Tamayo
 * @date	2/10/2019
 *
 */
public class StringUtil {

	public static String addLeftZeros(String input, int cant){
		String output = input;
		int zeros = cant - input.length();
		if(zeros > 0){			
			for(int i = 0; i < zeros; i++){
				output = '0' + output;
			}
		}
		return output;
	}
	
	public static String convertComposedStringToSeparatedString(
			String value, String separator, boolean isStartUpperCase) {
		if (isStartUpperCase) 
			value = value.toLowerCase();
		//
		String result = StringPool.BLANK;
		//
		for (int index = 0; index < value.length(); index++) {
			if (Character.isUpperCase(value.charAt(index))) {
				String word = String.valueOf(
						value.charAt(index));
				if (!isStartUpperCase)
					word = word.toLowerCase();
				//
				result += separator + word;
			} else {
				String word = String.valueOf(value.charAt(index));
				//
				if (isStartUpperCase) {
					if (index == 0)
						word = word.toUpperCase();
				}
				//
				result += word;
			}
		}
		//
		return result;
	}
	//
	public static String removeLeftZeros(String input) {
		return input.replaceAll("^0*", "");
	}
	//
	public static String validateToBlank(String input) {
		if (input == null) {
			input = StringPool.BLANK;
		}
		//
		return input;
	}
}
