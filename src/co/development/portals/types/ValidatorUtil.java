package co.development.portals.types;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;

/** 
 * 
 * @author H. Tamayo 
 * @last_edition 23/10/2018
 *
 */
public class ValidatorUtil {
	//
	/** 
	 * Return the first not null value. The default value is 0.0
	 * @param values
	 * @return
	 */
	public static Double getValidatedValue(
			Double... values) {
		Double result = 0.0; 
		//
		for (Double value : values) {
			if (Validator.isNotNull(value)) {
				result = value; 
				// 
				break;
			} 
		} 
		// 
		return result; 
	}
	//
	/** 
	 * Return the first not null value. The default value is 0
	 * @param values
	 * @return
	 */
	public static Integer getValidatedValue(
			Integer... values) {
		Integer result = 0; 
		//
		for (Integer value : values) {
			if (Validator.isNotNull(value)) {
				result = value; 
				// 
				break;
			} 
		} 
		// 
		return result; 
	}
	//
	/** 
	 * Return the first not null value. The default value is BLANK
	 * @param values
	 * @return
	 */
	public static String getValidatedValue(
			String... values) {
		String result = StringPool.BLANK; 
		//
		for (String value : values) {
			if (Validator.isNotNull(value)) {
				result = value; 
				// 
				break;
			} 
		} 
		// 
		return result; 
	}
	//
}
