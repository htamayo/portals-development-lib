package co.development.portals.types.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import co.development.portals.types.ObjectMapper;

/**
 * Implemented in field that must be excluded from mapping in 
 * {@link ObjectMapper}
 * 
 * @author H. Tamayo
 * @editor H. Tamayo
 * @last_edition 14/06/2017
 *
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MappingExclude {

}
