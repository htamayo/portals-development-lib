package co.development.portals.types;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.json.JSONSerializer;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

import co.development.portals.types.ObjectMapper;
import co.development.portals.types.annotation.JSONObjectField;

/**
 * 
 * @author 	H. Tamayo
 * @date	27/07/2019
 *
 */
public class JSONMapper {
	//
	public static <T> T getJSONValue(Class<T> fieldType, 
			String fieldName, JSONObject jsonObject) 
					throws NoSuchMethodException, SecurityException, 
					IllegalAccessException, IllegalArgumentException,
					InvocationTargetException {
		Method method = JSONObject.class.getDeclaredMethod(
				JSON_METHODS_BY_CLASS.get(fieldType), 
						String.class);
		//
		method.setAccessible(true);
		Object object = method.invoke(jsonObject, fieldName);
		//
		return fieldType.cast(object);
	}
	//
	public static <T> List<T> mapJSONArray(Class<T> clazz, 
			JSONArray jsonArray) throws IllegalAccessException, 
					IllegalArgumentException, InstantiationException,
					InvocationTargetException, JSONException, 
					NoSuchMethodException, SecurityException {
		List<T> list = new ArrayList<T>();
		//
		for (int i=0; i<jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			//
			T object = mapJSONObject(clazz, jsonObject);
			//
			list.add(object);
		}
		//
		return list;
	}
	//
	public static <T> T mapJSONObject(Class<T> clazz, 
			JSONObject jsonObject, Class<?>... arrayClasses) 
					throws IllegalAccessException, 
					IllegalArgumentException, InstantiationException,
					InvocationTargetException, JSONException, 
					NoSuchMethodException, SecurityException { 
		Object object = clazz.newInstance();
		Field[] fields = clazz.getDeclaredFields();
		//
		for (Field field : fields) {
			field.setAccessible(true);
			//
			if (ObjectMapper.isAcceptableField(field, false)) {
				Object value = getJSONValue(field.getType(), 
						field.getName(), jsonObject);
						
				//
				field.set(object, value);
			} else if(field.getType().equals(List.class)) {
				for (Class<?> arrayClass : arrayClasses) {
					ParameterizedType parameterizedType = 
							(ParameterizedType)
							field.getGenericType();
					Type innerType = 
							parameterizedType.getActualTypeArguments(
									)[0];
					Class<?> innerClass = (Class<?>)innerType;
					//
					if (innerClass.equals(arrayClass)) {
						JSONArray jsonArray = 
								jsonObject.getJSONArray(
										field.getName());
						//
						@SuppressWarnings("rawtypes")
						List<?> list = new ArrayList();
						//
						if (jsonArray != null) {
						  list = mapJSONArray(innerClass, jsonArray);
						}
						//
						field.set(object, list);
						//
						break;
					} else {
						continue;
					}
				}
			} else {
				_log.warn("Ooops! The Field: " + field.getName() 
						+ " with Type: " 
						+ field.getType().getSimpleName() + " is "
								+ "not supported, Sorry.");
			}
		}
		//
		return clazz.cast(object);
	}
	//
	public static JSONObject mapObject(Object object) 
			throws NoSuchMethodException, SecurityException, 
			IllegalAccessException, IllegalArgumentException, 
			InvocationTargetException {
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		//
		Class<?> clazz = object.getClass();
		Field[] fields = clazz.getDeclaredFields();
		//
		for (Field field : fields) {
			field.setAccessible(true);
			//
			if (field.isAnnotationPresent(
					JSONObjectField.class)) {
				Object innerObject = field.get(object);
				//
				JSONObject innerJSONObject = 
						mapObject(innerObject);
				//
				jsonObject.put(field.getName(), innerJSONObject);
			} else if (ObjectMapper.isAcceptableField(field)) {
				Object value = field.get(object);
				//
				jsonObject.put(field.getName(), String.valueOf(
						value));
			} // FIXME: Else with Inner JSON Array
		}
		//
		return jsonObject;
	}
	//
	public static <T> JSONArray mapObjects(List<T> list) 
			throws NoSuchMethodException, SecurityException, 
			IllegalAccessException, IllegalArgumentException, 
			InvocationTargetException {
		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		//
		for (T object : list) {
			JSONObject jsonObject = mapObject(object);
			//
			jsonArray.put(jsonObject);
		}
		//
		return jsonArray;
	}
	//
	public static JSONObject serializeObject(Object object) {
		try {
			JSONSerializer jsonSerializer = 
					JSONFactoryUtil.createJSONSerializer();
			String jsonObject = jsonSerializer.serialize(object);
			//
			return JSONFactoryUtil.createJSONObject(jsonObject);
		} catch (Exception ex) {
			String msg = "Error passing object to JSONObject "
					+ ": " + ex.getMessage();
			//
			if (_log.isDebugEnabled()) 
				_log.error(msg, ex);
			else 
				_log.error(msg);
			//
			return null; 
		}
	}
	//
	public static Map<Class<?>, String> WRAPPER_ALIAS;
	static {
		Map<Class<?>, String> map = new HashMap<Class<?>, String>();
		//
		map.put(Integer.class, "Int");
		//
		WRAPPER_ALIAS = Collections.unmodifiableMap(map);
	}
	//
	public static Map<Class<?>, String> JSON_METHODS_BY_CLASS;
	static {
		Map<Class<?>, String> map = new HashMap<Class<?>, String>();
		//
		for (Class<?> clazz : ObjectMapper.ACCEPTED_CLASSES) {
			Class<?> finalClazz = null;
			if (clazz.isPrimitive()) 
				finalClazz = ObjectMapper.wrapPrimitive(clazz);
			else 
				finalClazz = clazz;
			//
			String simpleName = StringPool.BLANK;
			if (WRAPPER_ALIAS.containsKey(finalClazz)) {
				simpleName = WRAPPER_ALIAS.get(finalClazz);
			} else {
				simpleName = finalClazz.getSimpleName();
			}
			//
			map.put(clazz, "get" + simpleName);
		}
		//
		JSON_METHODS_BY_CLASS = Collections.unmodifiableMap(map);
	}
	
	private static Log _log = LogFactoryUtil.getLog(
			JSONMapper.class);
}
