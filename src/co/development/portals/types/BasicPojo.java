package co.development.portals.types;

import com.liferay.portal.kernel.util.StringPool;

/**
 * 
 * @author 	H. Tamayo
 * @date	23/08/2019
 *
 */
public class BasicPojo {
	// 
	private String key;
	private String body;
	//
	public BasicPojo(String key, String body) {
		this.key = key;
		this.body = body;
	} 
	// 
	public BasicPojo(String key, boolean body) { 
		this.key = key; 
		this.body = String.valueOf(body);
	}
	//
	public BasicPojo(String key, double body) {
		this.key = key;
		this.body = String.valueOf(body);
	}
	// 
	public BasicPojo(String key, int body) { 
		this.key = key; 
		this.body = String.valueOf(body); 
	}
	// 
	public BasicPojo(String key, long body) { 
		this.key = key; 
		this.body = String.valueOf(body); 
	}
	//
	public BasicPojo() {
		this(StringPool.BLANK, 
				StringPool.BLANK);
	}
	//
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
}
