package co.development.portals.types;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.LocalTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.format.DateTimeFormatter;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * For a while, support for Threeten lib. 
 * 
 * @author H. Tamayo
 * @last_edition 20/05/2019
 *
 */
public class DateUtil {
	//
	private static Log _log = LogFactoryUtil.getLog(DateUtil.class);
	private static SimpleDateFormat formatter = 
			new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat dateTimeFormatter = 
			new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	//
	public static Calendar asCalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		//
		return calendar;
	}
	//
	public static Calendar asCalendar(LocalDate localDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		//
		calendar.set(Calendar.YEAR, localDate.getYear());
		calendar.set(Calendar.MONTH, localDate.getMonthValue());
		calendar.set(Calendar.DAY_OF_MONTH, 
				localDate.getDayOfMonth());
		//
		return calendar;
	}
	//
	public static Date asDate(java.sql.Date sqlDate) {
		return (Date)sqlDate;
	}
	//
	public static Date asDate(LocalDate localDate) {
		return DateTimeUtils.toDate(localDate.atStartOfDay(
				).atZone(ZoneId.systemDefault()).toInstant());
	}
	//
	public static Date asDate(LocalDate localDate, 
			LocalTime localTime) {
		return DateTimeUtils.toDate(localDate.atTime(
				localTime).atZone(ZoneId.systemDefault(
						)).toInstant());
	}
	//
	public static Date asDate(LocalDateTime localDateTime) {
		return DateTimeUtils.toDate(localDateTime.atZone(
				ZoneId.systemDefault()).toInstant());
	}
	//
	public static Date asDate(String date) {
		try {
			return formatter.parse(date);
		} catch(Exception ex) {
			String msg = "Error parsing Date from String: "
					+ ex.getMessage();
			//
			if (_log.isDebugEnabled()) 
				_log.error(msg, ex);
			else 
				_log.error(msg);
			//
			return null;
		}
	}
	//
	//
	public static Date asDateTime(String date) {
		try {
			return dateTimeFormatter.parse(date);
		} catch(Exception ex) {
			String msg = "Error parsing Date Time from String: "
					+ ex.getMessage();
			//
			if (_log.isDebugEnabled()) 
				_log.error(msg, ex);
			else 
				_log.error(msg);
			//
			return null;
		}
	}
	//
	public static Date asDateFromSqlDate(LocalDate localDate) {
		return asDate(DateTimeUtils.toSqlDate(localDate));
	}
	//
	public static LocalDate asLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(
				ZoneId.systemDefault()).toLocalDate();
	}
	//
	public static LocalDateTime asLocalDateTime(Date date) {
		return LocalDateTime.ofInstant(
				DateTimeUtils.toInstant(date), 
				ZoneId.systemDefault());
	}
	//
	public static boolean dateIsExpired(Date date)
		throws NullPointerException {
		Date currentDate = new Date();
		//
		if (date != null) {
			long days = (currentDate.getTime() - date.getTime());
			if (days > 1) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new NullPointerException(
					"Date to evaluate is null");
		}
	}
	//
	public static String toDTString(Date date) {
		return dateTimeFormatter.format(date);
	}
	//
	public static String toString(Date date) {
		return formatter.format(date);
	}
	//
	public static String toString(LocalDate localDate) { 
		if (localDate != null) { 
			DateTimeFormatter formatter = 
					DateTimeFormatter.ofPattern("yyyy-MM-dd");
			return formatter.format(localDate);
		} else {
			return "NullArgument"; 
		}
	}
	//
}
