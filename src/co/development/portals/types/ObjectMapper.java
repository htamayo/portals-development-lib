package co.development.portals.types;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;

import co.development.portals.types.annotation.MappingExclude;

/**
 * 
 * @author H. Tamayo
 * @last_edition 8/08/2017
 *
 */
public class ObjectMapper {

	public static <T> T mapObject(Class<T> clazz, Object[] object) 
			throws InstantiationException, IllegalAccessException, 
			NoSuchMethodException, SecurityException, 
			IllegalArgumentException, InvocationTargetException {
		return mapObject(clazz, object, null);
	}
	
	public static <T> T mapObject(Class<T> clazz, Object[] object, 
			AtomicInteger index) throws InstantiationException, 
					IllegalAccessException, NoSuchMethodException, 
					SecurityException, IllegalArgumentException, 
					InvocationTargetException {
		Constructor<T> constructor = 
				clazz.getDeclaredConstructor();
		constructor.setAccessible(true);
		Object obj = constructor.newInstance();
		Field[] fields = clazz.getDeclaredFields();
		//
		if (index == null)
			index = new AtomicInteger(0);
		//
		for (Field field : fields) { 
			field.setAccessible(true);
			if (!field.isAnnotationPresent(MappingExclude.class)) {
				//
				if (isAcceptableField(field)) {
					Object value = getValue(field.getType(), 
							object[index.get()]);
					//
					field.set(obj, value);
					//
					index.incrementAndGet();
				}
			}
		}
		//
		return clazz.cast(obj);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getValue(Class<T> fieldType, Object object) 
			throws NoSuchMethodException, SecurityException, 
			IllegalAccessException, IllegalArgumentException, 
			InvocationTargetException {
		Method method = GetterUtil.class.getMethod(
				getGetterMethodName(fieldType), Object.class);
		// Method invoke has null parameter because is a static method.
		Object result = method.invoke(null, object);
		// Necessary to cast between primitive and wrapper classes
		return (T)result;
	}
	
	public static boolean isAcceptableField(Field field) {
		return isAcceptableField(field, true);
	}
	
	public static boolean isAcceptableField(Field field, 
			boolean log) {
		boolean isAccepted = isAcceptableClassForField(
				field.getType());
		//
		if (!isAccepted && log)
			_log.warn("Ooop! The Field: " + field.getName() + 
					" with Type: " + field.getType().getSimpleName(
							) + " is not supported, Sorry.");
		//
		return isAccepted;
	}
	//
	public static boolean isAcceptableClassForField(
			Class<?> clazz) {
		return ACCEPTED_CLASSES.contains(clazz);
	}
	//
	public static List<Class<?>> ACCEPTED_CLASSES = 
			new ArrayList<Class<?>>();
	static {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		//
		classes.add(String.class);
		classes.add(Integer.class);
		classes.add(int.class);
		classes.add(Double.class);
		classes.add(double.class);
		classes.add(Long.class);
		classes.add(long.class);
		classes.add(Boolean.class);
		classes.add(boolean.class);
		//
		ACCEPTED_CLASSES = Collections.unmodifiableList(
				classes);
	}
	
	public static Class<?> wrapPrimitive(Class<?> primitiveClazz) {
		return PRIMITIVE_TO_WRAPPER.get(primitiveClazz);
	}
	
	private static Map<Class<?>, Class<?>> PRIMITIVE_TO_WRAPPER = 
			new HashMap<Class<?>, Class<?>>();
	static {
		Map<Class<?>, Class<?>> map = 
				new HashMap<Class<?>, Class<?>>();
		//
		map.put(int.class, Integer.class);
		map.put(double.class, Double.class);
		map.put(long.class, Long.class);
		map.put(boolean.class, Boolean.class);
		//
		PRIMITIVE_TO_WRAPPER = Collections.unmodifiableMap(map);
	}
	
	public static String getGetterMethodName(Class<?> clazz) {
		return GETTER_METHOD_BY_CLASS.get(clazz);
	}
	
	private static Map<Class<?>, String> GETTER_METHOD_BY_CLASS = 
			new HashMap<Class<?>, String>();
	static {
		Map<Class<?>, String> map = new HashMap<Class<?>, String>();
		//
		for (Class<?> clazz : ACCEPTED_CLASSES) {
			Class<?> finalClazz = null;
			if (clazz.isPrimitive()) {
				finalClazz = wrapPrimitive(clazz);
			} else {
				finalClazz = clazz;
			}
			//
			String simpleName = finalClazz.getSimpleName();
			//
			map.put(clazz, "get" + simpleName);
		}
		//
		GETTER_METHOD_BY_CLASS = Collections.unmodifiableMap(map);
	}
	
	private static Log _log = LogFactoryUtil.getLog(
			ObjectMapper.class);
}