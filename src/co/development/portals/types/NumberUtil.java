package co.development.portals.types;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import com.liferay.portal.kernel.util.StringPool;

/**
 * 
 * @author H. Tamayo 
 * @last_edition 7/06/2019
 *
 */
public class NumberUtil {
	private NumberUtil() {
		
	}
	//
	public static double doubleFromCurrency(double number, 
			String currency) {
		if (USD_CURRENCY.equals(currency)) {
			return doubleFromUSD(number);
		} else {
			return doubleFromCOP(number);
		}
	}
	//
	public static double doubleFromCurrency(String number, 
			String currency) {
		if (USD_CURRENCY.equals(currency)) {
			return doubleFromUSD(number);
		} else {
			return doubleFromCOP(number);
		}
	}
	//
	public static double doubleFromCOP(double number) {
		String strNumber = String.valueOf(
				number);
		//
		strNumber = integerNumber.format(number);
		//
		return doubleFromCOP(strNumber);
	}
	//
	public static double doubleFromCOP(String number) {
		number = number.replace(StringPool.DOLLAR, 
				StringPool.BLANK);
		number = number.replace(StringPool.PERIOD, 
				StringPool.BLANK);
		//
		return Double.valueOf(number);
	}
	//
	public static double doubleFromUSD(double number) {
		String strNumber = String.valueOf(
				number);
		//strNumber = strNumber.replace('.', ',');
		strNumber = decimalNumber.format(number);
		//
		return doubleFromUSD(strNumber);
	}
	//
	public static double doubleFromUSD(String number) {
		number = number.replace(StringPool.DOLLAR, 
				StringPool.BLANK);
		number = number.replace(StringPool.PERIOD, 
				StringPool.BLANK);
		//
		number = number.replace(StringPool.COMMA, 
				StringPool.PERIOD);
		//
		return Double.valueOf(number);
	}
	// 
	public static String formatCurrency(BigDecimal number, 
			String currency) {
		return formatCurrency(number.doubleValue(), 
				currency); 
	}
	//
	public static String formatCurrency(Double number, 
			String currency) {
		if (USD_CURRENCY.equals(currency)) {
			return formatUSDNumber(number);
		} else {
			return formatCOPNumber(number);
		}
	}
	//
	public static String formatNumber(BigDecimal number, 
			String currency) {
		return formatNumber(number.doubleValue(), 
				currency);
	}
	//
	public static String formatNumber(Double number, 
			String currency) {
		if (USD_CURRENCY.equals(currency)) {
			return formatDecimalNumber(number);
		} else {
			return formatIntegerNumber(number);
		}
	}
	//
	public static String formatCOPNumber(Double number) {
		return copFormat.format(number);
	}
	public static String formatUSDNumber(Double number) {
		return usdFormat.format(number);
	}
	//
	public static String formatCOPNumber(BigDecimal number) {
		return copFormat.format(number);
	}
	public static String formatUSDNumber(BigDecimal number) {
		return usdFormat.format(number);
	}
	//
	public static String formatDecimalNumber(Double number) {
		return decimalNumber.format(number);
	}
	public static String formatIntegerNumber(Double number) {
		return integerNumber.format(number);
	}
	//
	public static DecimalFormatSymbols symbols;
	public static NumberFormat copFormat;
	public static NumberFormat decimalNumber;
	public static NumberFormat integerNumber;
	public static NumberFormat usdFormat;
	//
	public static final String COP_CURRENCY = "COP";
	public static final String USD_CURRENCY = "USD";
	// Initializer
	static {
		copFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("ES"));
		copFormat.setMaximumFractionDigits(0);
		(((DecimalFormat) copFormat).getDecimalFormatSymbols()).setCurrencySymbol("");
		DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat)copFormat).getDecimalFormatSymbols();
		decimalFormatSymbols.setCurrencySymbol("");
		((DecimalFormat) copFormat).setDecimalFormatSymbols(decimalFormatSymbols);
		//
		decimalNumber = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("ES"));
		decimalNumber.setGroupingUsed(false);
		decimalNumber.setMaximumFractionDigits(2);
		(((DecimalFormat) decimalNumber).getDecimalFormatSymbols()).setCurrencySymbol("");
		decimalFormatSymbols = ((DecimalFormat)decimalNumber).getDecimalFormatSymbols();
		decimalFormatSymbols.setCurrencySymbol("");
		((DecimalFormat) decimalNumber).setDecimalFormatSymbols(decimalFormatSymbols);
		//
		integerNumber = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("ES"));
		integerNumber.setGroupingUsed(false);
		integerNumber.setMaximumFractionDigits(0);
		(((DecimalFormat) integerNumber).getDecimalFormatSymbols()).setCurrencySymbol("");
		decimalFormatSymbols = ((DecimalFormat)integerNumber).getDecimalFormatSymbols();
		decimalFormatSymbols.setCurrencySymbol("");
		((DecimalFormat) integerNumber).setDecimalFormatSymbols(decimalFormatSymbols);
		//
		usdFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("ES"));
		usdFormat.setMaximumFractionDigits(2);
		(((DecimalFormat) usdFormat).getDecimalFormatSymbols()).setCurrencySymbol("");
		decimalFormatSymbols = ((DecimalFormat)usdFormat).getDecimalFormatSymbols();
		decimalFormatSymbols.setCurrencySymbol("");
		((DecimalFormat) usdFormat).setDecimalFormatSymbols(decimalFormatSymbols);
	}
}
